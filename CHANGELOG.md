## 1.0.0
ohos 正式版本
 * 适配最新SDK6
 
## 0.0.1-SNAPSHOT
ohos 第一个版本
 * 实现了原库的大部分 api
 * 因为 ohos api 缺失 没有实现 手势下滑关闭图片 
 * 因为 外部依赖库fresco加载gif功能缺陷 没有实现加载gif图
 * 因为 外部依赖库fresco加载uri图片功能缺陷 没有实现图片未加载完成时无占位图片

