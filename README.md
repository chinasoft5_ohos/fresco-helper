# fresco-helper

#### 项目介绍
- 项目名称：fresco-helper
- 所属系列：openharmony 第三方组件适配移植
- 功能：让使用Fresco就像使用Glide、Picasso一样简洁是个不错的图片加载神器
- 项目移植状态：主功能完成
- 调用差异：有  (手势向下拖动关闭图片,加载gif图功能和图片未加载完成时无占位图片)
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 2.4.0

#### 效果演示
![fresco-helper](images/demo.PNG)  
![fresco-helper](images/demo1.PNG)  
![fresco-helper](images/photo_wall.PNG)
![fresco-helper](images/README.gif)

#### 安装教程
1.在项目根目录下的build.gradle文件中,
``` java
allprojects {
  repositories {
    maven {
         url 'http://106.15.92.248:8081/repository/Releases/'
    }
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
    } 
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/releases/'
    }
 }
}
```
2.在entry模块的build.gradle文件中
``` java
dependencies {
    implementation 'com.gitee.chinasoft_ohos:fresco-helper:1.0.0'
    implementation 'com.alexvasilkov.ohos:gesture-views:1.0.1'
    implementation 'io.openharmony.tpc.thirdlib:fresco:1.0.2'
    implementation 'io.openharmony.tpc.thirdlib:animated_base:1.0.0'
    implementation 'io.openharmony.tpc.thirdlib:animated_gif_lite:1.0.3'
    implementation 'io.openharmony.tpc.thirdlib:subsampling-scale-image-view:1.0.4'
    implementation 'com.gitee.chinasoft_ohos:commontools:0.0.5-SNAPSHOT'
    implementation 'io.openharmony.tpc.thirdlib:PhotoView:1.1.1'
    ......
}
```

在sdk6，DevEco Studio2.2 Beta1 下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
把这个  implementation 'com.gitee.chinasoft5_ohos:fresco-helper:0.0.1-SNAPSHOT' 加到你的 entry `build.gradle`
或者下载project并将其作为库插入.  

初始化Phoenix
```
Phoenix.init(this); // Context
```

从网络加载一张图片
```
String url = "http://ww3.sinaimg.cn/large/610dc034jw1f6m4aj83g9j20zk1hcww3.jpg";
SimpleDraweeView simpleDraweeView = (SimpleDraweeView)findComponentById(ResourceTable.Id_sdv_1);
Phoenix.with(simpleDraweeView).load(url);
```

从网络加载一张图片，按原图等比缩放显示
```
String url = "http://ww3.sinaimg.cn/large/610dc034jw1f6m4aj83g9j20zk1hcww3.jpg";
SimpleDraweeView simpleDraweeView = (SimpleDraweeView)findComponentById(ResourceTable.Id_sdv_1);
Phoenix.with(simpleDraweeView)
.setControllerListener(new SingleImageControllerListener(simpleDraweeView))
.load(url);
```

从网络加载一张图片，按原图等比缩放显示，添加限制最大宽高值（在指定的区域内等比缩放）
```
String url = "https://ws1.sinaimg.cn/large/0065oQSqly1fymj13tnjmj30r60zf79k.jpg";
SimpleDraweeView simpleDraweeView = (SimpleDraweeView)findComponentById(ResourceTable.Id_sdv_1);
Phoenix.with(simpleDraweeView)
       .setControllerListener(new SingleImageControllerListener(simpleDraweeView,
                        DensityUtil.vpToPixels(this, 200), DensityUtil.vpToPixels(this, 200)))
       .load(url);
```

从网络加载一张图片，想加上自己的后处理
```
String url = "http://ww3.sinaimg.cn/large/610dc034jw1f6m4aj83g9j20zk1hcww3.jpg";
SimpleDraweeView simpleDraweeView = (SimpleDraweeView)findComponentById(R.id.sdv_1);
Phoenix.with(simpleDraweeView)
       .setBasePostprocessor(new BasePostprocessor() {
            @Override
            public void process(PixelMap PixelMap) {
                // 后处理，比如做高斯模糊处理
                
            }
        }).load(url);
```
从res下面加载一张图片
```
SimpleDraweeView simpleDraweeView = (SimpleDraweeView)findComponentById(ResourceTable.Id_sdv_1);
Phoenix.with(simpleDraweeView).load(ResourceTable.Media_meizi);
```

在写布局文件xml时，我不知道要显示的图片尺寸，需要根据业务逻辑动态的设置要显示的图片的大小，可以像下面这样写：
```
String url = "http://ww3.sinaimg.cn/large/610dc034jw1f6m4aj83g9j20zk1hcww3.jpg";
SimpleDraweeView simpleDraweeView = (SimpleDraweeView)findComponentById(ResourceTable.Id_sdv_1);
Phoenix.with(simpleDraweeView)
       .setWidth(100)
       .setHeight(100)
       .load(url);
```
图片的高斯模糊处理
```
String url = "http://ww3.sinaimg.cn/large/610dc034jw1f6m4aj83g9j20zk1hcww3.jpg";
SimpleDraweeView simpleDraweeView = (SimpleDraweeView)findComponentById(ResourceTable.Id_sdv_1);
Phoenix.with(simpleDraweeView)
       .setNeedBlur(true)
       .load(url);
```

将图片资源预加载到磁盘缓存
```
String url = "http://ww1.sinaimg.cn/large/610dc034jw1fahy9m7xw0j20u00u042l.jpg";
Phoenix.prefetchToDiskCache(url);
```

将图片资源预加载到内存缓存
```
String url = "http://ww1.sinaimg.cn/large/610dc034jw1fahy9m7xw0j20u00u042l.jpg";
Phoenix.prefetchToPixelMapCache(url);
```

从网络下载一张图片，下载完成后把PixelMap给我（返回的结果是在主线程）
```
String url = "http://feed.chujianapp.com/20161108/452ab5752287a99a1b5387e2cd849006.jpg@1080w";
Phoenix.with(context)
       .setUrl(url)
       .setResult(new IResult<PixelMap>() {
            @Override
            public void onResult(PixelMap result) {
                // 在主线程

            }
        }).load();
```
从网络下载一张图片，下载完成后告诉我下载的图片存在哪里（返回的结果是在工作线程，非主线程）
```
String url = "http://feed.chujianapp.com/20161108/452ab5752287a99a1b5387e2cd849006.jpg@1080w";
String filePath = "";
Phoenix.with(context)
       .setUrl(url)
       .setResult(new IDownloadResult(filePath) {
           @Override
           public void onResult(String filePath) {
               MLog.i("filePath = " + filePath);
               // 在子线程，要显示到View上，需要切换到主线程

           }
       }).download(); 
```
从本地磁盘缓存中获取PixelMap
```
String url = "https://ws1.sinaimg.cn/large/610dc034ly1fgi3vd6irmj20u011i439.jpg";
Phoenix.with(url)
       .setResult(new IResult<PixelMap>() {

           @Override
           public void onResult(PixelMap result) {
              // 在主线程

              }
           }).loadLocalDiskCache();
```
从网络加载一张超大图
大小以M为单位，目标图片宽度大于手机屏幕宽的2倍以上或者高度大于手机屏幕高的2倍以上
```
<com.facebook.fresco.helper.photoview.LargePhotoView
        ohos:id="$+id:image"
        ohos:height="match_parent"
        ohos:width="match_parent" />
```

从网络加载并显示
```
String url = "http://img5q.duitang.com/uploads/item/201402/24/20140224212510_eQRG5.thumb.700_0.jpeg";
final LargePhotoView imageView = (LargePhotoView) findComponentById(ResourceTable.Id_image);
Phoenix.with(imageView).load(url);
``` 

设置缩放参数
```
imageView.setMinScale(1.0f);
imageView.setMaxRatio(2.0f);
```

禁用缩放功能（你可能会有这个需求）
```
imageView.setZoomEnabled(false);
```

从网络加载，存放到指定的缓存目录并显示
```
String fileCacheDir = getCacheDir().getAbsolutePath();
MLog.i("fileCacheDir = " + fileCacheDir);
Phoenix.with(imageView)
       .setDiskCacheDir(fileCacheDir)
       .load(url);     
```
其它

获取已占用磁盘缓存的大小，已B为单位
```
long cacheSize = Phoenix.getMainDiskStorageCacheSize();
MLog.info("cacheSize = " + cacheSize);
```

从内存缓存中移除指定图片的缓存
```
String url = "http://ww1.sinaimg.cn/large/610dc034jw1fahy9m7xw0j20u00u042l.jpg";
Phoenix.evictFromMemoryCache(url);
```

从磁盘缓存中移除指定图片的缓存
```
String url = "http://ww1.sinaimg.cn/large/610dc034jw1fahy9m7xw0j20u00u042l.jpg";
Phoenix.evictFromDiskCache(url);
```

移除指定图片的所有缓存（包括内存+磁盘）
```
String url = "http://ww1.sinaimg.cn/large/610dc034jw1fahy9m7xw0j20u00u042l.jpg";
Phoenix.evictFromCache(url);
```

清空所有内存缓存
```
Phoenix.clearMemoryCaches();
```

清空所有磁盘缓存
```
Phoenix.clearDiskCaches();
```

清除所有缓存（包括内存+磁盘）
```
Phoenix.clearCaches();
```
#### 测试信息
CodeCheck代码测试无异常  
CloudTest代码测试无异常  
病毒安全检测通过  
当前版本demo功能与原组件基本无差异

#### 版本迭代
* 1.0.0

#### 版权和许可信息
```
Copyright (C) 2019 android_ls@163.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```
