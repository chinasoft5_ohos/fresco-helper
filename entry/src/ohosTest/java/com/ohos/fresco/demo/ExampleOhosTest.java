package com.ohos.fresco.demo;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * 测试例子
 *
 * @author dev
 * @since 2021-07-29
 */
public class ExampleOhosTest {
    /**
     * 测试包名称
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.ohos.fresco.demo", actualBundleName);
    }
}