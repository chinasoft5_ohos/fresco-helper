package com.ohos.fresco.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.ScrollView;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.multimodalinput.event.TouchEvent;

import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.drawable.Animatable;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.fresco.helper.ImageLoader;
import com.facebook.fresco.helper.Phoenix;
import com.facebook.fresco.helper.utils.MLog;
import com.facebook.imagepipeline.image.ImageInfo;

/**
 * 壁画基础使用能力
 *
 * @author dev
 * @date 2021-08-20
 */
public class FrescoBaseUseAbility extends Ability implements Component.TouchEventListener {
    private AnimatorProperty animatorProperty;
    private Image image;
    private Image simpleDraweeView;
    private String url;
    private DirectionalLayout frescoBaseLayout;
    private boolean isFirst = true;
    private boolean isInActive = false;
    private EventHandler eventHandler;
    private EventHandler eHanedler;

    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#313A99"));
        window.setStatusBarVisibility(Component.VISIBLE);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fresco_base_use);
        url = getString(ResourceTable.String_image9);
        eventHandler = new EventHandler(EventRunner.getMainEventRunner());
        eHanedler = new EventHandler(EventRunner.getMainEventRunner());
        ScrollView scrollView = (ScrollView) findComponentById(ResourceTable.Id_scrollView);
        frescoBaseLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_frescoBaseLayout);
        image = (Image) findComponentById(ResourceTable.Id_frescoImage);
        simpleDraweeView = (Image) findComponentById(ResourceTable.Id_sdv_11);

        eHanedler.postSyncTask(new Runnable() {
            @Override
            public void run() {
                // 从网络加载一张图片
                Phoenix.with((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_1)).load(url);

                // 网络加载一张图片，并以圆形显示
                ImageLoader.loadImage((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_2), url, new ControllerListener<ImageInfo>() {
                    @Override
                    public void onSubmit(String s, Object o) {
                    }

                    @Override
                    public void onFinalImageSet(String s, ImageInfo imageInfo, Animatable animatable) {
                    }

                    @Override
                    public void onIntermediateImageSet(String s, ImageInfo imageInfo) {
                    }

                    @Override
                    public void onIntermediateImageFailed(String s, Throwable throwable) {
                    }

                    @Override
                    public void onFailure(String s, Throwable throwable) {
                    }

                    @Override
                    public void onRelease(String s) {
                    }
                });

                // 网络加载一张图片，并以圆形加边框显示
                ImageLoader.loadImage((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_3), url);

                ImageLoader.loadImage((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_4), url);

                ImageLoader.loadImage((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_5), url);

                ImageLoader.loadImage((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_6), url);

                ImageLoader.loadImage((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_7), "");

                ImageLoader.loadImage((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_8), url);

                ImageLoader.loadImage((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_9), url);
                findComponentById(ResourceTable.Id_sdv_9).setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        MLog.info("按下效果");
                    }
                });

                ImageLoader.loadImage((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_10), url);
            }
        });

        animatorProperty = image.createAnimatorProperty();
        animatorProperty.rotate(360).setDuration(5000).setLoopedCount(AnimatorValue.INFINITE);
        frescoBaseLayout.setTouchEventListener(this);
        scrollView.setTouchEventListener(this);

    }

    private void loadImage() {
        simpleDraweeView.setPixelMap(ResourceTable.Media_icon);
        image.setVisibility(Component.HIDE);
        animatorProperty.stop();
        animatorProperty.reset();
    }

    @Override
    protected void onActive() {
        super.onActive();
        if (isInActive) {
            isInActive = false;
            if (animatorProperty != null && !animatorProperty.isRunning()) {
                image.setVisibility(Component.VISIBLE);
                animatorProperty.start();
                eventHandler.postTask(new Runnable() {
                    @Override
                    public void run() {
                        loadImage();
                    }
                }, 400);
            }
        }
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        isInActive = true;
        animatorProperty.stop();
        animatorProperty.reset();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (eventHandler != null) {
            eventHandler.removeAllEvent();
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                if (isFirst) {
                    isFirst = false;
                    if (animatorProperty != null && !animatorProperty.isRunning()) {
                        image.setVisibility(Component.VISIBLE);
                        animatorProperty.start();
                        eventHandler.postTask(new Runnable() {
                            @Override
                            public void run() {
                                loadImage();
                            }
                        }, 400);
                    }
                }
                break;
        }
        return false;
    }
}
