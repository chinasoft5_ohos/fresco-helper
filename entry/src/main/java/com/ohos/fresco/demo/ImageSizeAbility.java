package com.ohos.fresco.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;
import ohos.bundle.AbilityInfo;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.fresco.helper.ImageLoader;
import com.facebook.fresco.helper.utils.DensityUtil;

/**
 * 图像大小的能力
 *
 * @author dev
 * @date 2021-08-20
 */
public class ImageSizeAbility extends Ability {
    private static final String LANDSCAPE = "LANDSCAPE";
    private SimpleDraweeView simpleDraweeView;

    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#313A99"));
        window.setStatusBarVisibility(Component.VISIBLE);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fresco_big);

        String url = getString(ResourceTable.String_image10);
        simpleDraweeView = (SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_1);
        // ImageLoader.loadImage(simpleDraweeView, url, new SingleImageControllerListener(simpleDraweeView));

        ComponentContainer.LayoutConfig vpl = simpleDraweeView.getLayoutConfig();
        vpl.width = DensityUtil.getDisplayWidth(this) - DensityUtil.vpToPixels(this, 22);
        vpl.height = (int) (DensityUtil.getDisplayHeight(this) * 0.5);
        simpleDraweeView.setLayoutConfig(vpl);

        ImageLoader.loadImage(simpleDraweeView, url);

//        ImageLoader.loadImage(simpleDraweeView, url,
//                DensityUtil.getDisplayWidth(this) - DensityUtil.dipToPixels(this, 22),
//                DensityUtil.getDisplayWidth(this) - DensityUtil.dipToPixels(this, 20));
    }

    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
        ComponentContainer.LayoutConfig vpl = simpleDraweeView.getLayoutConfig();
        vpl.width = DensityUtil.getDisplayWidth(this) - DensityUtil.vpToPixels(this, 22);
        simpleDraweeView.setLayoutConfig(vpl);
    }
}
