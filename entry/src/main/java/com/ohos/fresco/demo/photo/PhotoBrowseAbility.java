package com.ohos.fresco.demo.photo;

import ohos.agp.components.Component;

import com.facebook.fresco.helper.photoview.PictureBrowseAbility;
import com.facebook.fresco.helper.photoview.entity.PhotoInfo;
import com.facebook.fresco.helper.utils.MLog;
import com.ohos.fresco.demo.ResourceTable;

/**
 * 照片浏览能力
 *
 * @author dev
 * @date 2021-08-20
 */
public class PhotoBrowseAbility extends PictureBrowseAbility {
    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_ability_photo_browse;
    }

    @Override
    protected void setupViews() {
        super.setupViews();
        (findComponentById(ResourceTable.Id_rl_top_deleted)).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                MLog.info("用户点击了删除按钮");
                MLog.info("mPhotoIndex = " + mPhotoIndex);

                PhotoInfo photoInfo = mItems.get(mPhotoIndex);
                MLog.info("originalUrl = " + photoInfo.getOriginalUrl());

            }
        });
    }

    @Override
    public void onLongClicked(Component view) {
        MLog.info("currentPosition = " + getCurrentPosition());

        PhotoInfo photoInfo = getCurrentPhotoInfo();
        MLog.info("current originalUrl = " + photoInfo.getOriginalUrl());

        super.onLongClicked(view);
    }
}
