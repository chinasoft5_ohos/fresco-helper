package com.ohos.fresco.demo.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

import com.facebook.fresco.helper.Phoenix;
import com.facebook.fresco.helper.utils.MLog;
import com.ohos.fresco.demo.ResourceTable;

/**
 * MainAbilitySlice
 *
 * @author dev
 * @since 2021-08-20
 */
public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        findComponentById(ResourceTable.Id_btn_load_local_bitmap).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.LoadDiskCacheImageAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_open_photo_wall).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.photo.PhotoWallAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_open_photo_album).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.photo.PhotoAlbumAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_base_use).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.FrescoBaseUseAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_clear_memory).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Phoenix.clearCaches();
                ((Button) findComponentById(ResourceTable.Id_btn_get_cache_size)).setText("获取已使用的缓存大小");
            }
        });

        findComponentById(ResourceTable.Id_btn_big).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.BigImageAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_big_big).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.BigBigImageAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_asset_image).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.AssetsAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_get_cache_size).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                long cacheSize = Phoenix.getMainDiskStorageCacheSize();
                MLog.info("cacheSize = " + cacheSize);
                ((Button) findComponentById(ResourceTable.Id_btn_get_cache_size))
                    .setText("缓存:" + (cacheSize / 1024) + "KB");
            }
        });

        findComponentById(ResourceTable.Id_btn_small).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.ImageSizeAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_gif).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.GifAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_meizi).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.MeiziAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_meizi_webp).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.WebpAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_use_databinding).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.UseDataBindingAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_blur).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.BlurAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_blur2).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.Blur2Ability").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_load_bitmap).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.PixelMapAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_gif_bitmap).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.GIFFirstFrameAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_scale).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("").withBundleName(getBundleName())
                    .withAbilityName("com.ohos.fresco.demo.SingleImageControllerDemoAbility").build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
