package com.ohos.fresco.demo;

import com.ohos.fresco.demo.slice.MainAbilitySlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;

/**
 * MainAbility
 *
 * @author dev
 * @date 2021-08-20
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#313A99"));
        window.setStatusBarVisibility(Component.VISIBLE);
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}
