package com.ohos.fresco.demo.photo;

import ohos.agp.components.Component;

import java.util.ArrayList;


/**
 * item点击监听器
 *
 * @author dev
 * @date 2021-08-20
 */
public interface OnItemClickListener<T> {
    /**
     * 在项目点击
     *
     * @param component 组件
     * @param photos 照片
     * @param position 位置
     */
    void onItemClick(Component component, ArrayList<T> photos, int position);
}
