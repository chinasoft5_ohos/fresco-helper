package com.ohos.fresco.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Window;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.TouchEvent;

import com.alexvasilkov.gestures.GestureDetector;
import com.facebook.fresco.helper.Phoenix;
import com.facebook.fresco.helper.listener.IResult;
import com.facebook.fresco.helper.photoview.LargePhotoView;
import com.facebook.fresco.helper.utils.MLog;

/**
 * 大图像的能力
 *
 * @author dev
 * @date 2021-08-20
 */
public class BigBigImageAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#313A99"));
        window.setStatusBarVisibility(Component.VISIBLE);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fresco_big_big);
        final LargePhotoView imageView = (LargePhotoView) findComponentById(ResourceTable.Id_image);
        imageView.setMinScale(1.0f);
        imageView.setMaxRatio(2.0f);
        String url = getString(ResourceTable.String_image2);
        Phoenix.with(imageView).load(url); // 不指定缓存目录
    }
}
