/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos.fresco.demo.photo;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.media.image.PixelMap;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.fresco.helper.Phoenix;
import com.facebook.fresco.helper.listener.IResult;
import com.facebook.fresco.helper.photoview.entity.PhotoInfo;
import com.facebook.fresco.helper.utils.DensityUtil;
import com.ohos.fresco.demo.ResourceTable;

import java.util.ArrayList;

/**
 * 照片墙适配器
 *
 * @author dev
 * @date 2021-08-20
 */
public class PhotoWallAdapter extends BaseItemProvider {
    private Ability mAbility;
    private ArrayList<PhotoInfo> mPhotos;
    private OnItemClickListener mOnItemClickListener;

    /**
     * 照片墙适配器
     *
     * @param ability 能力
     * @param photos 照片
     * @param onItemClickListener 在项点击监听器
     */
    public PhotoWallAdapter(Ability ability, ArrayList<PhotoInfo> photos, OnItemClickListener onItemClickListener) {
        this.mAbility = ability;
        this.mPhotos = photos;
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public int getCount() {
        int size = 0;
        if (mPhotos != null) {
            size = mPhotos.size();
        }
        return size;
    }

    @Override
    public PhotoInfo getItem(int position) {
        return mPhotos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    // 重写该方法即可避免复用布局出现错乱问题
    @Override
    public int getItemComponentType(int position) {
        return position;
    }

    // 重写改方法 布局类型的数量
    @Override
    public int getComponentTypeCount() {
        return 1;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        Componentholder viewholder;
        if (convertComponent == null) {
            convertComponent = LayoutScatter.getInstance(mAbility)
                .parse(ResourceTable.Layout_photo_wall_item, null, false);
            viewholder = new Componentholder(convertComponent);
            convertComponent.setTag(viewholder);
        } else {
            viewholder = (Componentholder) convertComponent.getTag();
        }

        int itemDimensionSize = (DensityUtil.getDisplayWidth(mAbility.getContext())
            - DensityUtil.vpToPixels(mAbility.getContext(), 5f)) / 4;
        ComponentContainer.LayoutConfig vlp = viewholder.dependentLayout.getLayoutConfig();
        vlp.width = itemDimensionSize;
        vlp.height = itemDimensionSize;
        viewholder.dependentLayout.setLayoutConfig(vlp);


        PhotoInfo photoInfo = mPhotos.get(position);
        String fileCacheDir = mAbility.getContext().getCacheDir().getAbsolutePath();
        Phoenix.with(mAbility)
            .setDiskCacheDir(fileCacheDir)
            .setSmallDiskCache(true)
            .build(photoInfo.getOriginalUrl())
            .setResult(new IResult<PixelMap>() {
                @Override
                public void onResult(PixelMap result) {
                    if (result != null) {
                        viewholder.mImage.setPixelMap(result);
                        viewholder.photoWall.setVisibility(Component.HIDE);
                    }
                }

                @Override
                public void onProgress(int progress) {
                }
            })
            .load();


        viewholder.mImage.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(component, mPhotos, position);
                }
            }
        });
        return convertComponent;
    }

    /**
     * holder
     */
    static class Componentholder {
        DependentLayout dependentLayout;
        Image photoWall;
        Image mImage;

        public Componentholder(Component convertComponent) {
            dependentLayout = (DependentLayout) convertComponent.findComponentById(ResourceTable.Id_dependentLayout);
            mImage = (Image) convertComponent.findComponentById(ResourceTable.Id_iv_thumbnail);
            photoWall = (Image) convertComponent.findComponentById(ResourceTable.Id_photoWall);
        }
    }

    /**
     * 刷新数据
     *
     * @param list
     */
    public void refreshPathList(ArrayList<PhotoInfo> list) {
        mPhotos = new ArrayList<PhotoInfo>(list);
        notifyDataChanged();
    }
}
