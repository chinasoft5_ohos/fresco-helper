package com.ohos.fresco.demo.photo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TableLayoutManager;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;

import com.facebook.fresco.helper.photoview.PhotoX;
import com.facebook.fresco.helper.photoview.entity.PhotoInfo;
import com.facebook.fresco.helper.statusbar.StatusBarCompat;
import com.ohos.fresco.demo.ResourceTable;

import java.util.ArrayList;

/**
 * 照片墙的能力
 *
 * @author dev
 * @date 2021-08-20
 */
public class PhotoWallAbility extends Ability {
    private ListContainer mRecyclerView;
    private PhotoWallAdapter mPhotoWallAdapter;
    private TableLayoutManager mLayoutManager;


    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#00000000"));
        window.setStatusBarVisibility(Component.VISIBLE);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_photo_wall);
        StatusBarCompat.translucentStatusBar(this);
        final String[] images = {
            // gif
            getString(ResourceTable.String_gif1),
            getString(ResourceTable.String_gif2),
            getString(ResourceTable.String_gif3),
            getString(ResourceTable.String_gif4),
            getString(ResourceTable.String_gif5),
            getString(ResourceTable.String_gif6),
            getString(ResourceTable.String_gif7),
            getString(ResourceTable.String_gif8),
            getString(ResourceTable.String_gif9),
            getString(ResourceTable.String_gif10),
            getString(ResourceTable.String_gif11),

            // normal
            getString(ResourceTable.String_gif12),
            getString(ResourceTable.String_gif13),
            getString(ResourceTable.String_gif14),
            getString(ResourceTable.String_gif15),
            getString(ResourceTable.String_gif16),
            getString(ResourceTable.String_gif17),
            getString(ResourceTable.String_gif18),
            getString(ResourceTable.String_gif19),
            getString(ResourceTable.String_gif20),
            getString(ResourceTable.String_gif21),
            getString(ResourceTable.String_gif22),
            getString(ResourceTable.String_gif23),
            getString(ResourceTable.String_gif24),
            getString(ResourceTable.String_gif25),
            getString(ResourceTable.String_gif26),
            getString(ResourceTable.String_gif27),
            getString(ResourceTable.String_gif28),
            getString(ResourceTable.String_gif29),
            getString(ResourceTable.String_gif30),
            getString(ResourceTable.String_gif31),
            getString(ResourceTable.String_gif32),
            getString(ResourceTable.String_gif33),
            getString(ResourceTable.String_gif34),
            getString(ResourceTable.String_gif35),
            getString(ResourceTable.String_gif36),
            getString(ResourceTable.String_gif37),
            getString(ResourceTable.String_gif38),
            getString(ResourceTable.String_gif39),
            getString(ResourceTable.String_gif40),
            getString(ResourceTable.String_gif41),
            getString(ResourceTable.String_gif42),
            getString(ResourceTable.String_gif43),
            getString(ResourceTable.String_gif44),
            getString(ResourceTable.String_gif45),
            getString(ResourceTable.String_gif46),
            getString(ResourceTable.String_gif47),
            getString(ResourceTable.String_gif48),
            getString(ResourceTable.String_gif49),
            getString(ResourceTable.String_gif50),
            getString(ResourceTable.String_gif51),
            getString(ResourceTable.String_gif52),
            getString(ResourceTable.String_gif53),
            getString(ResourceTable.String_gif54),
            getString(ResourceTable.String_gif55),
            getString(ResourceTable.String_gif56),
            getString(ResourceTable.String_gif57),
            getString(ResourceTable.String_gif58),
            getString(ResourceTable.String_gif59),
            getString(ResourceTable.String_gif60),
            getString(ResourceTable.String_gif61),
            getString(ResourceTable.String_gif62),
            getString(ResourceTable.String_gif63),
            getString(ResourceTable.String_gif64),
            getString(ResourceTable.String_gif65),
            getString(ResourceTable.String_gif66),
            getString(ResourceTable.String_gif67),
            getString(ResourceTable.String_gif68),
            getString(ResourceTable.String_gif69),
            getString(ResourceTable.String_gif70),

        };
        ArrayList<PhotoInfo> data = new ArrayList<>();
        for (String image : images) {
            PhotoInfo photoInfo = new PhotoInfo();
            photoInfo.setOriginalUrl(image);
            photoInfo.setThumbnailUrl(image);
            data.add(photoInfo);
        }

        PhotoInfo photoInfo = new PhotoInfo();
        String url = getString(ResourceTable.String_gif71);
        photoInfo.setOriginalUrl(url);
        photoInfo.setThumbnailUrl(url);
        photoInfo.setWidth(1416);
        photoInfo.setHeight(885);
        data.add(photoInfo);

        photoInfo = new PhotoInfo();
        url = getString(ResourceTable.String_gif72);
        photoInfo.setOriginalUrl(url);
        photoInfo.setThumbnailUrl(url);

        photoInfo.setWidth(3840);
        photoInfo.setHeight(2160);
        data.add(photoInfo);

        photoInfo = new PhotoInfo();
        url = getString(ResourceTable.String_gif73);
        photoInfo.setOriginalUrl(url);
        photoInfo.setThumbnailUrl(url);
        photoInfo.setWidth(1573);
        photoInfo.setHeight(885);
        data.add(photoInfo);

        photoInfo = new PhotoInfo();
        url = getString(ResourceTable.String_gif74);
        photoInfo.setOriginalUrl(url);
        photoInfo.setThumbnailUrl(url);
        photoInfo.setWidth(700);
        photoInfo.setHeight(7633);
        data.add(photoInfo);

        photoInfo = new PhotoInfo();
        url = getString(ResourceTable.String_gif75);
        photoInfo.setOriginalUrl(url);
        photoInfo.setThumbnailUrl(url);
        photoInfo.setWidth(776);
        photoInfo.setHeight(1240);
        data.add(photoInfo);

        mRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_rv_photo_wall);
        mLayoutManager = new TableLayoutManager();
        mLayoutManager.setColumnCount(4);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mPhotoWallAdapter = new PhotoWallAdapter(this, data, new OnItemClickListener<PhotoInfo>() {

            @Override
            public void onItemClick(Component view, ArrayList<PhotoInfo> photos, int position) {
//                MLog.i("position = " + position);
//                MLog.i("photos.get(position).thumbnailUrl = " + photos.get(position).thumbnailUrl);

                // 自定义浏览大图的UI
//                PhotoX.with(PhotoWallActivity.this, PhotoBrowseActivity.class)
//                        .setLayoutManager(mLayoutManager)
//                        .setPhotoList(photos)
//                        .setCurrentPosition(position)
//                        .enabledAnimation(true)
//                        .enabledDragClose(true)
//                        .toggleLongClick(false)
//                        .start();
                PhotoX.with(PhotoWallAbility.this)
                    .setLayoutManager(mRecyclerView)
                    .setPhotoList(photos)
                    .setCurrentPosition(position)
                    .enabledAnimation(true)
                    .enabledDragClose(true)
                    .start();

//                ArrayList<String> photoList = new ArrayList<>();
//                for (int i = 0; i < images.length; i++) {
//                    photoList.add(images[i]);
//                }
//
//                PhotoX.with(PhotoWallActivity.this)
//                        .setLayoutManager(mLayoutManager)
//                        .setPhotoStringList(photoList)
//                        .setCurrentPosition(position)
//                        .enabledAnimation(true)
//                        .enabledDragClose(true)
//                        .start();

//                PhotoX.with(PhotoWallActivity.this)
//                        .setPhotoList(photos)
//                        .setCurrentPosition(position)
//                        .start();

//                String originalUrl = photos.get(position).originalUrl;
//                PhotoX.with(PhotoWallActivity.this)
//                        .setThumbnailView(view)
//                        .setOriginalUrl(originalUrl)
//                        .enabledAnimation(true)
//                        .start();

//                PhotoX.with(PhotoWallActivity.this)
//                        .setThumbnailView(view)
//                        .setPhotoInfo(photos.get(position))
//                        .enabledAnimation(true)
//                        .start();

//                String originalUrl = photos.get(position).originalUrl;
//                PhotoX.with(PhotoWallActivity.this)
//                        .setOriginalUrl(originalUrl)
//                        .start();

            }
        });
        mRecyclerView.setItemProvider(mPhotoWallAdapter);
    }
}
