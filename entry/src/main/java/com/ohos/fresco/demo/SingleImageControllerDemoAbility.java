package com.ohos.fresco.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.fresco.helper.Phoenix;
import com.facebook.fresco.helper.controller.SingleImageControllerListener;
import com.facebook.fresco.helper.utils.DensityUtil;

/**
 * 单一图像控制器演示能力
 *
 * @author dev
 * @date 2021-08-20
 */
public class SingleImageControllerDemoAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#313A99"));
        window.setStatusBarVisibility(Component.VISIBLE);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fresco_scale);

        String url = getString(ResourceTable.String_image6);
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_1);
        ComponentContainer.LayoutConfig layoutConfig = simpleDraweeView.getLayoutConfig();
        layoutConfig.width = DensityUtil.vpToPixels(this, 150);
        layoutConfig.height = DensityUtil.vpToPixels(this, 200);
        simpleDraweeView.setLayoutConfig(layoutConfig);
        Phoenix.with(simpleDraweeView)
            .setControllerListener(new SingleImageControllerListener(simpleDraweeView,
                DensityUtil.vpToPixels(this, 200), DensityUtil.vpToPixels(this, 200)))
            .load(url);

        //        Phoenix.with(simpleDraweeView)
//                .setControllerListener(new SingleImageControllerListener(simpleDraweeView))
//                .load(url);
    }
}
