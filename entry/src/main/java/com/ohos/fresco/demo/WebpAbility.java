package com.ohos.fresco.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;
import ohos.bundle.AbilityInfo;

import com.facebook.fresco.helper.utils.DensityUtil;

/**
 * webp能力
 *
 * @author dev
 * @date 2021-08-20
 */
public class WebpAbility extends Ability {
    private static final String LANDSCAPE = "LANDSCAPE";
    private Component emptyComponent;

    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#313A99"));
        window.setStatusBarVisibility(Component.VISIBLE);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fresco_meizi);

        emptyComponent = findComponentById(ResourceTable.Id_emptyComponent);
        Image simpleDraweeView = (Image) findComponentById(ResourceTable.Id_sdv_1);

        ComponentContainer.LayoutConfig lvp = simpleDraweeView.getLayoutConfig();
        ComponentContainer.LayoutConfig layoutConfig = emptyComponent.getLayoutConfig();

        if (DensityUtil.getDisplayWidth(this) < DensityUtil.getDisplayHeight(this)) { // 竖屏
            lvp.width = (int) (DensityUtil.getDisplayWidth(this) * 0.55);
            simpleDraweeView.setLayoutConfig(lvp);

            layoutConfig.height = (int) (DensityUtil.getDisplayHeight(this) / 4.5);
        } else {
            lvp.width = (int) (DensityUtil.getDisplayHeight(this) * 0.5);
            simpleDraweeView.setLayoutConfig(lvp);

            layoutConfig.height = 0;
        }
        emptyComponent.setLayoutConfig(layoutConfig);
//        simpleDraweeView.setAspectRatio(0.6f); // 设置宽高比

        simpleDraweeView.setPixelMap(ResourceTable.Media_meizi);
//        ImageLoader.loadDrawable(simpleDraweeView, ResourceTable.Media_meizi_webp);

//        Phoenix.with(simpleDraweeView)
////                .setWidth(DensityUtil.getDisplayWidth(this))
//                .setHeight(DensityUtil.getDisplayWidth(this))
//                .setAspectRatio(0.64f)
//                .load("https://ww4.sinaimg.cn/large/610dc034jw1fb8iv9u08ij20u00u0tc7.jpg");
    }

    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
        String name = displayOrientation.name();
        ComponentContainer.LayoutConfig layoutConfig = emptyComponent.getLayoutConfig();
        if (LANDSCAPE.equals(name)) { // 横屏
            layoutConfig.height = 0;
        } else { // 竖屏
            layoutConfig.height = (int) (DensityUtil.getDisplayHeight(this) / 4.5);
        }
        emptyComponent.setLayoutConfig(layoutConfig);
    }
}
