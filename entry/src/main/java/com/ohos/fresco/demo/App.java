package com.ohos.fresco.demo;

import ohos.aafwk.ability.AbilityPackage;

import com.facebook.fresco.helper.Phoenix;
import com.facebook.fresco.helper.config.PhoenixConfig;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.listener.RequestListener;
import com.facebook.imagepipeline.listener.RequestLoggingListener;
import com.ohos.fresco.demo.okhttp3.OkHttpNetworkFetcher;

import java.util.HashSet;
import java.util.Set;

import okhttp3.OkHttpClient;

/**
 * 应用程序
 *
 * @author dev
 * @date 2021-08-20
 */
public class App extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        Phoenix.init(this);

        // LOG过滤标签： RequestLoggingListener
        Set<RequestListener> requestListeners = new HashSet<>();
        requestListeners.add(new RequestLoggingListener());

//        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
//        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
//                .addInterceptor(loggingInterceptor)
            .build();

        ImagePipelineConfig imagePipelineConfig = new PhoenixConfig.Builder(this)
            .setNetworkFetcher(new OkHttpNetworkFetcher(okHttpClient))
            .setRequestListeners(requestListeners)
            .build();

        Phoenix.init(this, imagePipelineConfig); // this-->Context
    }
}
