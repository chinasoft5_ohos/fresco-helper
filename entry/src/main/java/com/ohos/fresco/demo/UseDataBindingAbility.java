package com.ohos.fresco.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;

import com.facebook.drawee.view.SimpleDraweeView;
import com.ohos.fresco.demo.databinding.ImageBindingAdapter;

/**
 * 使用数据绑定能力
 *
 * @author dev
 * @date 2021-08-20
 */
public class UseDataBindingAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#313A99"));
        window.setStatusBarVisibility(Component.VISIBLE);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fresco_use_databinding);

        String url = getString(ResourceTable.String_image3);

        ImageBindingAdapter.loadImage((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_1), url);
        ImageBindingAdapter.loadImage((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_2), url);
        ImageBindingAdapter.loadImage((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_3), url);
        ImageBindingAdapter.loadImage((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_4), url);
        ImageBindingAdapter.loadImage((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_5), url);
        ImageBindingAdapter.loadImage((SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_6), url);
    }
}
