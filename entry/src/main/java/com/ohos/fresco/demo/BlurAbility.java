package com.ohos.fresco.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.fresco.helper.ImageLoader;
import com.facebook.fresco.helper.utils.DensityUtil;

/**
 * 模糊的能力
 *
 * @author dev
 * @date 2021-08-20
 */
public class BlurAbility extends Ability {
    private static final float FLOAT = 0.76f;
    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#313A99"));
        window.setStatusBarVisibility(Component.VISIBLE);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fresco_blur);

        String url = getString(ResourceTable.String_image8);

        SimpleDraweeView simpleDraweeView = (SimpleDraweeView)findComponentById(ResourceTable.Id_sdv_1);
        Image image = (Image) findComponentById(ResourceTable.Id_image);

        simpleDraweeView.setAspectRatio(0.7f);

        ComponentContainer.LayoutConfig lvp = simpleDraweeView.getLayoutConfig();
        lvp.width = DensityUtil.getDisplayWidth(this);
        lvp.height = (int) (DensityUtil.getDisplayHeight(this) * FLOAT);
        simpleDraweeView.setLayoutConfig(lvp);

        ComponentContainer.LayoutConfig layoutConfig = image.getLayoutConfig();
        layoutConfig.width = DensityUtil.getDisplayWidth(this);
        layoutConfig.height =  (int) (DensityUtil.getDisplayHeight(this) * FLOAT);
        image.setLayoutConfig(layoutConfig);

        ImageLoader.loadImageBlur(image,simpleDraweeView, url);
    }
}
