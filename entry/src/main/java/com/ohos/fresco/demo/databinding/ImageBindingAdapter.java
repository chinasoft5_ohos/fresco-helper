/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos.fresco.demo.databinding;

import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.media.image.PixelMap;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.fresco.helper.ImageLoader;
import com.facebook.fresco.helper.listener.IResult;
import com.facebook.fresco.helper.utils.DensityUtil;

/**
 * 图像适配器工具类
 *
 * @author dev
 * @since 2021-07-29
 */
public class ImageBindingAdapter {
    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final int TWO = 2;
    private static final int THREE = 3;

    private ImageBindingAdapter() {
    }

    /**
     * 加载图片
     *
     * @param simpleDraweeView 简单的付款人视图
     * @param url url
     */
    public static void loadImage(SimpleDraweeView simpleDraweeView, String url) {
        ImageLoader.loadImage(simpleDraweeView, url);
    }

    /**
     * 加载图像小
     *
     * @param simpleDraweeView 简单的付款人视图
     * @param url url
     */
    public static void loadImageSmall(SimpleDraweeView simpleDraweeView, String url) {
        ImageLoader.loadImageSmall(simpleDraweeView, url);
    }

    /**
     * 加载文本可拉的
     *
     * @param view 视图
     * @param url url
     * @param iconWidth 图标的宽度
     * @param iconHeight 图标的高度
     */
    public static void loadTextDrawable(final Text view, String url, int iconWidth, int iconHeight) {
        ImageLoader.loadImage(view.getContext(), url, new IResult<PixelMap>() {
            @Override
            public void onResult(PixelMap bitmap) {
                Element drawable = new PixelMapElement(bitmap);
                final int width = DensityUtil.vpToPixels(view.getContext(), iconWidth);
                final int height = DensityUtil.vpToPixels(view.getContext(), iconHeight);
                drawable.setBounds(0, 0, width, height);
                view.setAroundElements(drawable, null, null, null);
            }

            @Override
            public void onProgress(int progress) {
            }
        });
    }

    /**
     * 加载文本可拉的
     *
     * @param view 视图
     * @param url url
     * @param direction 方向
     * @param iconWidth 图标的宽度
     * @param iconHeight 图标的高度
     */
    public static void loadTextDrawable(Text view, String url, int direction, int iconWidth, int iconHeight) {
        ImageLoader.loadImage(view.getContext(), url, new IResult<PixelMap>() {
            @Override
            public void onResult(PixelMap bitmap) {
                Element drawable = new PixelMapElement(bitmap);
                final int width = DensityUtil.vpToPixels(view.getContext(), iconWidth);
                final int height = DensityUtil.vpToPixels(view.getContext(), iconHeight);
                drawable.setBounds(0, 0, width, height);
                switch (direction) {
                    case ZERO:
                        view.setAroundElements(drawable, null, null, null);
                        break;
                    case ONE:
                        view.setAroundElements(null, drawable, null, null);
                        break;
                    case TWO:
                        view.setAroundElements(null, null, drawable, null);
                        break;
                    case THREE:
                        view.setAroundElements(null, null, null, drawable);
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onProgress(int progress) {
            }
        });
    }
}
