package com.ohos.fresco.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.utils.Color;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.Window;

import com.facebook.fresco.helper.ImageLoader;
import com.facebook.fresco.helper.utils.DensityUtil;

/**
 * blur2能力
 *
 * @author dev
 * @date 2021-08-20
 */
public class Blur2Ability extends Ability {
    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#313A99"));
        window.setStatusBarVisibility(Component.VISIBLE);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fresco_blur2);

        String url = getString(ResourceTable.String_image4);
        Component component =findComponentById(ResourceTable.Id_ll_bg);
        ImageLoader.loadImageBlur(component, url);
    }
}
