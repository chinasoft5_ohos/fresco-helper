package com.ohos.fresco.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import com.facebook.fresco.helper.utils.DensityUtil;
import com.oszc.bbhmlibrary.utils.LogUtil;

import java.io.IOException;
import java.io.InputStream;

import static com.facebook.fresco.helper.utils.FileUtils.TAG;


/**
 * Assets
 *
 * @author dev
 * @since 2021-08-19
 */
public class AssetsAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#313A99"));
        window.setStatusBarVisibility(Component.VISIBLE);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fresco_assets);

        // TODO 网络图片缓存清空导致无法加载图片，此处为替代方案
//        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_1);
        Image simpleDraweeView = (Image) findComponentById(ResourceTable.Id_sdv_1);

        DirectionalLayout directionalLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_includeLayout);
        /*Phoenix.with(simpleDraweeView)
            .setAssets(true)
            .load("qingchun.jpg");
*/

        ComponentContainer.LayoutConfig lvp = simpleDraweeView.getLayoutConfig();
        lvp.width = DensityUtil.getDisplayWidth(this);
        lvp.height = DensityUtil.getDisplayHeight(this) - directionalLayout.getHeight();
        simpleDraweeView.setLayoutConfig(lvp);

        // TODO 网络图片缓存清空导致无法加载图片，此处为替代方案
//        ImageLoader.loadDrawable(simpleDraweeView, ResourceTable.Media_qingchun,
//            DensityUtil.getDisplayWidth(this), DensityUtil.getDisplayHeight(this) - directionalLayout.getHeight());
        simpleDraweeView.setPixelMap(getPixelMapFromResource(ResourceTable.Media_qingchun));
    }

    /**
     * 通过图片ID返回PixelMap
     *
     * @param resourceId 图片的资源ID
     * @return 图片的PixelMap
     */
    private PixelMap getPixelMapFromResource(int resourceId) {
        InputStream inputStream = null;
        PixelMap pixelMap = null;
        try {
            // 创建图像数据源ImageSource对象
            inputStream = getResourceManager().getResource(resourceId);
            ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
            srcOpts.formatHint = "image/png";
            ImageSource imageSource = ImageSource.create(inputStream, srcOpts);

            // 设置图片参数
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            pixelMap = imageSource.createPixelmap(decodingOptions);
            return pixelMap;
        } catch (IOException e) {
            LogUtil.info(TAG, "IOException");
        } catch (NotExistException e) {
            LogUtil.info(TAG, "NotExistException");
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    LogUtil.info(TAG, "inputStream IOException");
                }
            }
        }
        return pixelMap;
    }
}
