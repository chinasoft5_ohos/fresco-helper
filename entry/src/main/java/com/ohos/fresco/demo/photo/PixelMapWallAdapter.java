/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos.fresco.demo.photo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.PixelMapElement;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import ohos.utils.net.Uri;

import com.facebook.fresco.helper.Phoenix;
import com.facebook.fresco.helper.listener.IResult;
import com.facebook.fresco.helper.photoview.entity.PhotoInfo;
import com.facebook.fresco.helper.utils.DensityUtil;
import com.ohos.fresco.demo.ResourceTable;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * 像素地图墙适配器
 *
 * @author dev
 * @date 2021-08-20
 */
public class PixelMapWallAdapter extends BaseItemProvider {
    private Ability mAbility;
    private ArrayList<PhotoInfo> mPhotos;
    private OnItemClickListener mOnItemClickListener;

    public PixelMapWallAdapter(Ability ability, ArrayList<PhotoInfo> photos, OnItemClickListener onItemClickListener) {
        this.mAbility = ability;
        this.mPhotos = photos;
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public int getCount() {
        int size = 0;
        if (mPhotos != null) {
            size = mPhotos.size();
        }
        return size;
    }

    @Override
    public Object getItem(int position) {
        return mPhotos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    // 重写该方法即可避免复用布局出现错乱问题
    @Override
    public int getItemComponentType(int position) {
        return position;
    }

    // 重写改方法 布局类型的数量
    @Override
    public int getComponentTypeCount() {
        return 1;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        Componentholder viewholder;
        if (null == convertComponent) {
            convertComponent = LayoutScatter.getInstance(mAbility)
                .parse(ResourceTable.Layout_pixelMap_wall_item, null, false);
            viewholder = new Componentholder(convertComponent);
            convertComponent.setTag(viewholder);
        } else {
            viewholder = (Componentholder) convertComponent.getTag();
        }

        int itemDimensionSize = (DensityUtil.getDisplayWidth(viewholder.mImage.getContext())
            - DensityUtil.vpToPixels(viewholder.mImage.getContext(), 18f)) / 4;
        ComponentContainer.LayoutConfig vlp = viewholder.mImage.getLayoutConfig();
        vlp.width = itemDimensionSize;
        vlp.height = itemDimensionSize;
        viewholder.mImage.setLayoutConfig(vlp);

        PhotoInfo photoInfo = mPhotos.get(position);
        Phoenix.with(mAbility)
            .setWidth(itemDimensionSize)
            .setHeight(itemDimensionSize)
            .setUrl(photoInfo.getUri())
            .setResult(new IResult<PixelMap>() {
                @Override
                public void onResult(PixelMap result) {
                    if (result != null) {
                        viewholder.mImage.setPixelMap(result);
                    }
                }
                @Override
                public void onProgress(int progress) {
                }
            }).load();

        viewholder.mImage.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(component, mPhotos, position);
                }
            }
        });

        return convertComponent;
    }

    /**
     * holder
     */
    static class Componentholder {
        Image mImage;

        /**
         * componentholder
         *
         * @param component 组件
         */
        Componentholder(Component component) {
            mImage = (Image) component.findComponentById(ResourceTable.Id_iv_thumbnail);
        }
    }

    /**
     * 刷新数据
     *
     * @param list
     */
    public void refreshPathList(ArrayList<PhotoInfo> list) {
        mPhotos = new ArrayList<PhotoInfo>(list);
        notifyDataChanged();
    }

}
