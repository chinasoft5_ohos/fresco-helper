package com.ohos.fresco.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;

import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.drawable.Animatable;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.fresco.helper.Phoenix;
import com.facebook.fresco.helper.utils.DensityUtil;
import com.facebook.fresco.helper.utils.MLog;
import com.facebook.imagepipeline.image.ImageInfo;

/**
 * 大图像的能力
 *
 * @author dev
 * @date 2021-08-20
 */
public class BigImageAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#313A99"));
        window.setStatusBarVisibility(Component.VISIBLE);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fresco_big);
        final String url = getString(ResourceTable.String_image5);
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView)findComponentById(ResourceTable.Id_sdv_1);

//        RoundingParams roundingParams = new RoundingParams();
//        roundingParams.setRoundAsCircle(true);
//        roundingParams.setBorder(Color.RED, 20);
//
//        roundingParams.setBorderColor(ContextCompat.getColor(simpleDraweeView.getContext(),
//                R.color.default_background_color));
////        roundingParams.setBorderColor(Color.parseColor("#0xff0000"));
//        roundingParams.setBorderWidth(23f);
//
//        GenericDraweeHierarchy genericDraweeHierarchy
//                = GenericDraweeHierarchyBuilder.newInstance(simpleDraweeView.getResources())
//                .setRoundingParams(roundingParams)
//                .build();
//        simpleDraweeView.setHierarchy(genericDraweeHierarchy);


        Phoenix.with(simpleDraweeView)
            .setWidth(DensityUtil.getDisplayWidth(this))
            .setHeight(DensityUtil.getDisplayHeight(this))
            .setControllerListener(new ControllerListener<ImageInfo>() {
                @Override
                public void onSubmit(String id, Object callerContext) {
                    MLog.info("onSubmit id = " + id);

                }

                @Override
                public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                    MLog.info("onFinalImageSet id = " + id);

                }

                @Override
                public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
                    MLog.info("onIntermediateImageSet id = " + id);

                }

                @Override
                public void onIntermediateImageFailed(String id, Throwable throwable) {
                    MLog.info("onIntermediateImageFailed id = " + id);

                }

                @Override
                public void onFailure(String id, Throwable throwable) {
                    MLog.info("onFailure id = " + id);

                }

                @Override
                public void onRelease(String id) {
                    MLog.info("onRelease id = " + id);

                }
            }).load(url);
    }
}
