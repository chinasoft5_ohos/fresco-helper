package com.ohos.fresco.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.drawable.Animatable;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.fresco.helper.Phoenix;
import com.facebook.fresco.helper.listener.IResult;
import com.facebook.fresco.helper.utils.MLog;
import com.facebook.imagepipeline.image.ImageInfo;

/**
 * 加载磁盘高速缓存图像的能力
 *
 * @author dev
 * @date 2021-08-20
 */
public class LoadDiskCacheImageAbility extends Ability {
    private Image mImageView;
    private String mUrl;

    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#313A99"));
        window.setStatusBarVisibility(Component.VISIBLE);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fresco_local_disk_cache);

        mUrl = getString(ResourceTable.String_image7);
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) findComponentById(ResourceTable.Id_sdv_1);
        mImageView = (Image) findComponentById(ResourceTable.Id_local_image);

        MLog.info("isInDiskCacheSync = " + Phoenix.isInDiskCacheSync(Uri.parse(mUrl)));

        Phoenix.with(simpleDraweeView)
            .setControllerListener(new ControllerListener<ImageInfo>() {
                @Override
                public void onSubmit(String id, Object callerContext) {
                    MLog.info("onSubmit id = " + id);

                }

                @Override
                public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                    MLog.info("onFinalImageSet id = " + id);

                    if (Phoenix.isInDiskCacheSync(Uri.parse(mUrl))) {
                        MLog.info("isInDiskCacheSync=true");
                        Phoenix.with(mUrl)
                            .setResult(new IResult<PixelMap>() {

                                @Override
                                public void onResult(PixelMap result) {
                                    MLog.info("onResult bitmap");
                                    mImageView.setPixelMap(result);
                                }

                                @Override
                                public void onProgress(int progress) {
                                }
                            }).loadLocalDiskCache();
                    }

                }

                @Override
                public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
                    MLog.info("onIntermediateImageSet id = " + id);

                }

                @Override
                public void onIntermediateImageFailed(String id, Throwable throwable) {
                    MLog.info("onIntermediateImageFailed id = " + id);

                }

                @Override
                public void onFailure(String id, Throwable throwable) {
                    MLog.info("onFailure id = " + id);

                }

                @Override
                public void onRelease(String id) {
                    MLog.info("onRelease id = " + id);

                }
            }).load(mUrl);
    }
}
