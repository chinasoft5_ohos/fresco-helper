package com.ohos.fresco.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.fresco.helper.ImageLoader;
import com.facebook.fresco.helper.utils.DensityUtil;

/**
 * 像素映射能力
 *
 * @author dev
 * @date 2021-08-20
 */
public class PixelMapAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(Color.getIntColor("#313A99"));
        window.setStatusBarVisibility(Component.VISIBLE);
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fresco_pixelmap);

        SimpleDraweeView imageView = (SimpleDraweeView) findComponentById(ResourceTable.Id_iv_thumbnail);
//        Image imageView = (Image) findComponentById(ResourceTable.Id_iv_thumbnail);
        ComponentContainer.LayoutConfig layoutConfig = imageView.getLayoutConfig();
        layoutConfig.width = DensityUtil.vp2px(this, 180);
        layoutConfig.height = DensityUtil.vp2px(this, 180);
        imageView.setLayoutConfig(layoutConfig);
        String url = getString(ResourceTable.String_image1);
        ImageLoader.loadImage(imageView,url);
//        Phoenix.with(this)
//            .setUrl(url)
//            .setWidth(DensityUtil.vp2px(this, 180))
//            .setHeight(DensityUtil.vp2px(this, 180))
//            .setCircleBitmap(true) // 圆形的
//            .setResult(new IResult<PixelMap>() {
//                @Override
//                public void onResult(PixelMap pixelMap) {
//                    MLog.info("Thread.currentThread().getId() = " + Thread.currentThread().getId());
//
//                    imageView.setPixelMap(pixelMap);
//                }
//            })
//            .load();
    }
}
