/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.listener;

/**
 * 结果监听
 *
 * @author dev
 * @param <T> 子类泛型
 * @since 2021-08-02
 */
public interface IResult<T> {
    /**
     * 结果
     *
     * @param result 结果
     */
    void onResult(T result);

    /**
     * 结果进展
     *
     * @param progress 进步
     */
    void onProgress(int progress);
}
