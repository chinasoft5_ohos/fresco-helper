/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.photoview.anim;

import ohos.agp.animation.Animator;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentParent;

/**
 * 动画侦听器
 *
 * @author dev
 * @since 2021-08-02
 */
public class AnimListener implements Animator.StateChangedListener {
    private Component mFromView;
    private Component mToView;

    /**
     * 动画侦听器
     *
     * @param fromView 从视图
     * @param toView 查看
     */
    public AnimListener(Component fromView, Component toView) {
        this.mFromView = fromView;
        this.mToView = toView;
    }

    /**
     * 在开始
     *
     * @param animator 动画师
     */
    @Override
    public void onStart(Animator animator) {
        mFromView.setVisibility(Component.VISIBLE);
    }

    /**
     * 在停止
     *
     * @param animator 动画师
     */
    @Override
    public void onStop(Animator animator) {
    }

    /**
     * 在取消
     *
     * @param animator 动画师
     */
    @Override
    public void onCancel(Animator animator) {
    }

    /**
     * 在结束
     *
     * @param animator 动画师
     */
    @Override
    public void onEnd(Animator animator) {
        mFromView.setVisibility(Component.INVISIBLE);
        ((ComponentParent) mFromView.getComponentParent()).removeComponent(mFromView);

        if (mToView != null) {
            mToView.setVisibility(Component.VISIBLE);
        }
    }

    /**
     * 在暂停
     *
     * @param animator 动画师
     */
    @Override
    public void onPause(Animator animator) {
    }

    /**
     * 在简历
     *
     * @param animator 动画师
     */
    @Override
    public void onResume(Animator animator) {
    }
}
