/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.facebook.fresco.helper.utils;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 新增
 * AttrSet的util工具类
 *
 * @since 2021-08-02
 */
public class AttrUtil {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "AttrUtil");

    private AttrUtil() {
    }

    /**
     * 返回自定义int类型的颜色值
     *
     * @param attrs AttrSet类
     * @param attrName 自定义属性名
     * @param defValue 默认值
     * @return 自定义int类型的颜色值
     */
    public static int getIntColor(AttrSet attrs, String attrName, int defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getColorValue().getValue();
        } else {
            return defValue;
        }
    }

    /**
     * 返回自定义Color类型的颜色值
     *
     * @param attrs AttrSet类
     * @param attrName 自定义属性名
     * @param defValue 默认值
     * @return 自定义Color类型的颜色值
     */
    public static Color getColor(AttrSet attrs, String attrName, Color defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getColorValue();
        } else {
            return defValue;
        }
    }

    /**
     * 返回自定义布尔值
     *
     * @param attrs AttrSet类
     * @param attrName 自定义属性名
     * @param defValue 默认值
     * @return 自定义布尔值
     */
    public static boolean getBoolean(AttrSet attrs, String attrName, boolean defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getBoolValue();
        } else {
            return defValue;
        }
    }

    /**
     * 返回自定义字符串值
     *
     * @param attrs AttrSet类
     * @param attrName 自定义属性名
     * @param defValue 默认值
     * @return 自定义字符串值
     */
    public static String getString(AttrSet attrs, String attrName, String defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getStringValue();
        } else {
            return defValue;
        }
    }

    /**
     * 返回自定义int值
     *
     * @param attrs AttrSet类
     * @param attrName 自定义属性名
     * @param defValue 默认值
     * @return 自定义value值
     */
    public static int getInt(AttrSet attrs, String attrName, int defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getIntegerValue();
        } else {
            return defValue;
        }
    }

    /**
     * 返回自定义float值
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return return
     */
    public static float getFloat(AttrSet attrs, String attrName, float defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getFloatValue();
        } else {
            return defValue;
        }
    }

    /**
     * 返回自定义Element值
     *
     * @param attrs AttrSet类
     * @param attrName 自定义属性名
     * @param defValue 默认Element
     * @return 自定义layout值
     */
    public static Element getElement(AttrSet attrs, String attrName, Element defValue) {
        if (attrs.getAttr(attrName) != null && attrs.getAttr(attrName).isPresent()) {
            return attrs.getAttr(attrName).get().getElement();
        } else {
            return defValue;
        }
    }
}
