/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.photoview.anim;

import ohos.aafwk.ability.Ability;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

/**
 * 现场扩大动画师
 *
 * @author dev
 * @since 2021-08-02
 */
public class SceneScaleUpAnimator extends TransitionAnimator {
    private float mStartX;
    private float mStartY;
    private float mHeight;
    private float mWidth;

    /**
     * 过渡动画
     *
     * @param activity 活动
     * @param viewOptions 视图选项
     * @param component 组件
     */
    public SceneScaleUpAnimator(Ability activity, Component component, ViewOptions viewOptions) {
        super(activity,component);
        mStartX = viewOptions.getStartX();
        mStartY = viewOptions.getStartY();
        mWidth = viewOptions.getWidth();
        mHeight = viewOptions.getHeight();
    }

    /**
     * 屏幕玩动物
     *
     * @param isEnter 是输入
     */
    public void playScreenAnim(final boolean isEnter) {
        Component sceneRoot = getSceneRoot();
        float fromAlpha;
        float toAlpha;
        float fromX;
        float toX;
        float fromY;
        float toY;
        float fromScaleX;
        float toScaleX;
        float fromScaleY;
        float toScaleY;

        if (isEnter) {
            fromAlpha = 1f;
            toAlpha = 1f;

            fromX = mStartX;
            toX = 0f;
            fromY = mStartY;
            toY = 0f;

            fromScaleX = (float) mWidth / getSceneRoot().getWidth();
            toScaleX = 1f;
            fromScaleY = (float) mHeight / getSceneRoot().getHeight();
            toScaleY = 1f;
        } else {
            fromAlpha = 1f;
            toAlpha = 1f;

            fromX = 0f;
            toX = mStartX;
            fromY = 0f;
            toY = mStartY;

            fromScaleX = 1f;
            toScaleX = (float) mWidth / getSceneRoot().getWidth();
            fromScaleY = 1f;
            toScaleY = (float) mHeight / getSceneRoot().getHeight();
        }
        sceneRoot.setPivotX(0f); // 定义移动的起始位置，如果不定义就是相对于自身中心
        sceneRoot.setPivotY(0f);

        AnimatorProperty animatorProperty = sceneRoot.createAnimatorProperty();
        animatorProperty.alphaFrom(fromAlpha).alpha(toAlpha)
            .moveFromX(fromX).moveToX(toX)
            .moveFromY(fromY).moveToY(toY)
            .scaleXFrom(fromScaleX).scaleX(toScaleX)
            .scaleYFrom(fromScaleY).scaleY(toScaleY);
        animatorProperty.setDuration(getAnimDuration());
        animatorProperty.setCurveType(Animator.CurveType.ACCELERATE);
        animatorProperty.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onStop(Animator animator) {
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
                if (isEnter) {
                    enterAnimsEnd();
                } else {
                    exitAnimsEnd();
                }
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        animatorProperty.start();
    }
}
