/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.photoview;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.app.Context;
import ohos.utils.net.Uri;

import com.facebook.fresco.helper.photoview.anim.ViewOptions;
import com.facebook.fresco.helper.photoview.anim.ViewOptionsCompat;
import com.facebook.fresco.helper.photoview.entity.PhotoInfo;
import com.facebook.fresco.helper.utils.MLog;
import com.facebook.fresco.helper.utils.PhotoConstant;
import com.oszc.bbhmlibrary.wrapper.TextUtils;

import java.util.ArrayList;

/**
 * photox
 *
 * @author dev
 * @since 2021-08-03
 */
public class PhotoX {
    private PhotoX() {
    }

    /**
     * 与
     *
     * @param context 上下文
     * @return {@link Builder}
     */
    public static Builder with(Context context) {
        return new Builder(context);
    }

    /**
     * 与
     *
     * @param context 上下文
     * @param targetClass 目标类
     * @return {@link Builder}
     */
    public static Builder with(Context context, Class<?> targetClass) {
        return new Builder(context, targetClass);
    }

    /**
     * 构建器
     *
     * @author dev
     * @since 2021-08-03
     */
    public static class Builder {
        private Intent mIntent;
        private ListContainer mLayoutManager;
        private ArrayList<String> mThumbnailList;
        private Component mThumbnailView;
        private String mOriginalUrl;
        private Context mContext;

        /**
         * 构建器
         *
         * @param context 上下文
         */
        private Builder(Context context) {
            mIntent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.ohos.fresco.demo")
                .withAbilityName(PictureBrowseAbility.class)
                .build();
            mIntent.setOperation(operation);
            mContext = context;
        }

        /**
         * 构建器
         *
         * @param context 上下文
         * @param targetClass 目标类
         */
        private Builder(Context context, Class<?> targetClass) {
            mIntent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(context.getBundleName())
                .withAbilityName(targetClass)
                .build();
            mIntent.setOperation(operation);
            mContext = context;
        }

        /**
         * 设置图片列表
         *
         * @param data 数据
         * @return {@link Builder}
         */
        public Builder setPhotoList(ArrayList<PhotoInfo> data) {
            int size = data.size();
            mThumbnailList = new ArrayList<>();
            for (int ii = 0; ii < size; ii++) {
                PhotoInfo photoInfo = data.get(ii);
                if (!TextUtils.isEmpty(photoInfo.getThumbnailUrl())) {
                    mThumbnailList.add(photoInfo.getThumbnailUrl());
                }
            }
            mIntent.setParam(PhotoConstant.PHOTO_LIST_KEY, data);
            return this;
        }

        /**
         * 设置图片列表
         *
         * @param data 数据
         * @param uris uri
         * @return {@link Builder}
         */
        public Builder setPhotoList(ArrayList<PhotoInfo> data, ArrayList<Uri> uris) {
            int size = data.size();
            mThumbnailList = new ArrayList<>();
            for (int ii = 0; ii < size; ii++) {
                PhotoInfo photoInfo = data.get(ii);
                if (!TextUtils.isEmpty(photoInfo.getThumbnailUrl())) {
                    mThumbnailList.add(photoInfo.getThumbnailUrl());
                }
            }
            mIntent.setParam(PhotoConstant.PHOTO_LIST_KEY, data);
            mIntent.setSequenceableArrayListParam(PhotoConstant.PHOTO_URI,uris);
            return this;
        }


        /**
         * 设置照片字符串列表
         *
         * @param data 数据
         * @return {@link Builder}
         */
        public Builder setPhotoStringList(ArrayList<String> data) {
            int size = data.size();
            mThumbnailList = new ArrayList<>();
            ArrayList<PhotoInfo> photos = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                String imageUrl = data.get(i);
                PhotoInfo photoInfo = new PhotoInfo();
                photoInfo.setOriginalUrl(imageUrl);
                photoInfo.setThumbnailUrl(imageUrl);
                photos.add(photoInfo);
                mThumbnailList.add(imageUrl);
            }
            mIntent.setParam(PhotoConstant.PHOTO_LIST_KEY, photos);
            return this;
        }

        /**
         * 原始url设置
         *
         * @param originalUrl 原始url
         * @return {@link Builder}
         */
        public Builder setOriginalUrl(String originalUrl) {
            mOriginalUrl = originalUrl;

            ArrayList<PhotoInfo> photos = new ArrayList<>();
            PhotoInfo photoInfo = new PhotoInfo();
            photoInfo.setOriginalUrl(originalUrl);
            photos.add(photoInfo);

            mIntent.setParam(PhotoConstant.PHOTO_LIST_KEY, photos);
            mIntent.setParam(PhotoConstant.PHOTO_ONLY_ONE_KEY, true);
            return this;
        }

        /**
         * 组照片信息
         *
         * @param photoInfo 照片信息
         * @return {@link Builder}
         */
        public Builder setPhotoInfo(PhotoInfo photoInfo) {
            mOriginalUrl = photoInfo.getOriginalUrl();

            ArrayList<PhotoInfo> photos = new ArrayList<>();
            photos.add(photoInfo);

            mIntent.setParam(PhotoConstant.PHOTO_LIST_KEY, photos);
            mIntent.setParam(PhotoConstant.PHOTO_ONLY_ONE_KEY, true);
            return this;
        }

        /**
         * 当前被点击的View在照片墙中的索引
         *
         * @param position 位置
         * @return {@link Builder}
         */
        public Builder setCurrentPosition(int position) {
            mIntent.setParam(PhotoConstant.PHOTO_CURRENT_POSITION_KEY, position);
            return this;
        }

        /**
         * 切换时间点
         *
         * @param onLongClick 在长时间点击
         * @return {@link Builder}
         */
        public Builder toggleLongClick(boolean onLongClick) {
            mIntent.setParam(PhotoConstant.PHOTO_LONG_CLICK_KEY, onLongClick);
            return this;
        }

        /**
         * 启用拖近
         *
         * @param isDragClose 拖近
         * @return {@link Builder}
         */
        public Builder enabledDragClose(boolean isDragClose) {
            mIntent.setParam(PhotoConstant.PHOTO_DRAG_CLOSE, isDragClose);
            return this;
        }

        /**
         * 使动画
         *
         * @param isAnimation 是动画
         * @return {@link Builder}
         */
        public Builder enabledAnimation(boolean isAnimation) {
            mIntent.setParam(PhotoConstant.PHOTO_ANIMATION_KEY, isAnimation);
            if (isAnimation) {
                if (mLayoutManager != null && mThumbnailList != null && mThumbnailList.size() > 0) {
                    Intent bundle = ViewOptionsCompat.makeScaleUpAnimation(mLayoutManager, mThumbnailList);
                    ArrayList<ViewOptions> view_option_list = bundle.getSerializableParam(ViewOptionsCompat.KEY_VIEW_OPTION_LIST);
                    mIntent.setParam(ViewOptionsCompat.KEY_VIEW_OPTION_LIST, view_option_list);
                } else if (mThumbnailView != null && mOriginalUrl != null) {
                    MLog.info("mOriginalUrl = " + mOriginalUrl);
                    Intent bundle = ViewOptionsCompat.makeScaleUpAnimation(mThumbnailView, mOriginalUrl);
                    ArrayList<ViewOptions> view_option_list = bundle.getSerializableParam(ViewOptionsCompat.KEY_VIEW_OPTION_LIST);
                    mIntent.setParam(ViewOptionsCompat.KEY_VIEW_OPTION_LIST, view_option_list);
                }
            }
            return this;
        }

        /**
         * 设置布局管理器
         *
         * @param layoutManager 布局管理器
         * @return {@link Builder}
         */
        public Builder setLayoutManager(ListContainer layoutManager) {
            this.mLayoutManager = layoutManager;
            return this;
        }

        /**
         * 设置缩略图
         *
         * @param thumbnailView 缩略图视图
         * @return {@link Builder}
         */
        public Builder setThumbnailView(Component thumbnailView) {
            this.mThumbnailView = thumbnailView;
            return this;
        }

        /**
         * 开始
         */
        public void start() {
            mContext.startAbility(mIntent, 1);
            ((Ability) mContext).setTransitionAnimation(0, 0);
        }
    }
}
