/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.photoview.anim;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.utils.Rect;

/**
 * 视图动画
 *
 * @author dev
 * @since 2021-08-02
 */
public class ViewAnim {
    private static final long DURATION = 300L;
    private long mDuration = DURATION;
    private AnimListener mAnimListener;

    /**
     * 开始查看简单的动画
     *
     * @param fromView 从视图
     * @param finalBounds 最后的界限
     * @param startOffsetY 开始offsety
     * @param finalOffsetY 最后offsety
     * @param startAlpha 开始α
     * @param finalAlpha 最后α
     */
    public void startViewSimpleAnim(final Component fromView, Rect finalBounds, int
        startOffsetY, int finalOffsetY, float startAlpha, float finalAlpha) {

        int[] position;
        position = fromView.getLocationOnScreen();

        Rect startBounds = new Rect();
        startBounds.left = position[0];
        startBounds.top = position[1];
        startBounds.right = startBounds.left + fromView.getWidth();
        startBounds.bottom = startBounds.top + fromView.getHeight();

        // 设置偏移量
        startBounds.offset(0, -startOffsetY);
        finalBounds.offset(0, -finalOffsetY);

        // 设定拉伸或者旋转动画的中心位置，这里是相对于自身左上角
        fromView.setPivotX(0f);
        fromView.setPivotY(0f);

        // 计算拉伸比例
        float scaleX = (float) finalBounds.getWidth() / startBounds.getWidth();
        float scaleY = (float) finalBounds.getHeight() / startBounds.getHeight();

        AnimatorProperty animatorProperty = fromView.createAnimatorProperty();
        animatorProperty.alphaFrom(startAlpha).alpha(finalAlpha)
            .moveFromX(startBounds.left).moveToX(finalBounds.left)
            .moveFromY(startBounds.top).moveFromY(finalBounds.top)
            .scaleXFrom(1f).scaleX(scaleX)
            .scaleYFrom(1f).scaleY(scaleY);
        animatorProperty.setDuration(mDuration);
        animatorProperty.setStateChangedListener(mAnimListener);
        animatorProperty.start();
    }

    /**
     * 设置动画差值器
     *
     * @param interpolator
     */
    public void setTimeInterpolator(int interpolator) {
    }

    /**
     * 得到时间
     *
     * @return long
     */
    public long getDuration() {
        return mDuration;
    }

    /**
     * 设置时间
     *
     * @param time 时间
     */
    public void setDuration(long time) {
        mDuration = time;
    }

    /**
     * 添加动画侦听器
     *
     * @param animListener 动画侦听器
     */
    public void addAnimListener(AnimListener animListener) {
        this.mAnimListener = animListener;
    }
}
