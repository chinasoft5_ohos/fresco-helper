/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.controller;

import ohos.agp.components.ComponentContainer;

import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.drawable.Animatable;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.fresco.helper.utils.DensityUtil;
import com.facebook.imagepipeline.image.ImageInfo;

/**
 * 单一图像控制器监听器
 *
 * @author dev
 * @since 2021-07-29
 */
public class SingleImageControllerListener extends BaseControllerListener<ImageInfo> {
    private static final int WITH = 1080;
    private static final int HEIGHT = 1920;
    private final SimpleDraweeView draweeView;
    private final int mMaxWidth;
    private final int mMaxHeight;

    /**
     * 单一图像控制器监听器
     *
     * @param draweeView 付款人的观点
     * @param maxWidth 最大宽度
     * @param maxHeight 最大高度
     */
    public SingleImageControllerListener(SimpleDraweeView draweeView, int maxWidth, int maxHeight) {
        this.draweeView = draweeView;
        this.mMaxWidth = maxWidth;
        this.mMaxHeight = maxHeight;
    }

    /**
     * 单一图像控制器监听器
     *
     * @param draweeView 付款人的观点
     */
    public SingleImageControllerListener(SimpleDraweeView draweeView) {
        this(draweeView, 0, 0);
    }

    @Override
    public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
        if (imageInfo == null || draweeView == null) {
            return;
        }
        ComponentContainer.LayoutConfig vp = draweeView.getLayoutConfig();

        int maxWidthSize;
        int maxHeightSize;
        if (mMaxWidth > 0 && mMaxHeight > 0) {
            maxWidthSize = mMaxWidth;
            maxHeightSize = mMaxHeight;
        } else {
            maxWidthSize = DensityUtil.getDisplayWidth(draweeView.getContext());
            maxHeightSize = DensityUtil.getDisplayHeight(draweeView.getContext());
        }

        if (maxWidthSize > WITH) {
            maxWidthSize = WITH;
        }

        if (maxHeightSize > HEIGHT) {
            maxHeightSize = HEIGHT;
        }
        int width = imageInfo.getWidth();
        int height = imageInfo.getHeight();

        if (width > height) {
            if (width > maxWidthSize) {
                width = maxWidthSize;
            }
            vp.width = width;
            vp.height = (int) (imageInfo.getHeight() / (float) imageInfo.getWidth() * vp.width);
        } else {
            // width <= height
            if (height > maxHeightSize) {
                height = maxHeightSize;
            }

            vp.height = height;
            vp.width = (int) ((float) imageInfo.getWidth() / imageInfo.getHeight() * vp.height);
        }

        draweeView.postLayout();
    }
}
