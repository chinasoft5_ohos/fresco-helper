/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.utils;

/**
 * 照片常数
 *
 * @author dev
 * @since 2021-08-03
 */
public class PhotoConstant {
    private PhotoConstant() {
    }

    /**
     * 照片列表关键
     */
    public static final String PHOTO_LIST_KEY = "photoData";
    /**
     * 照片当前位置的关键
     */
    public static final String PHOTO_CURRENT_POSITION_KEY = "position";
    /**
     * 照片动画关键
     */
    public static final String PHOTO_ANIMATION_KEY = "isAnimation";
    /**
     * 照片只有一个关键
     */
    public static final String PHOTO_ONLY_ONE_KEY = "onlyOne";
    /**
     * 照片长点击关键
     */
    public static final String PHOTO_LONG_CLICK_KEY = "onLongClick";
    /**
     * 照片拖近
     */
    public static final String PHOTO_DRAG_CLOSE = "isDragClose";

    /**
     * 照片uri
     */
    public static final String PHOTO_URI = "photoUri";


}
