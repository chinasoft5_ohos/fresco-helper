/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.photoview.entity;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;
import ohos.utils.net.Uri;

import java.io.Serializable;

/**
 * 照片信息
 *
 * @author dev
 * @since 2021-08-02
 */
public class PhotoInfo implements Serializable {
    /**
     * 照片的身份证
     */
    private String photoId;

    /**
     * 原始url
     */
    private String originalUrl;

    /**
     * 缩略图url
     */
    private String thumbnailUrl;

    /**
     * 宽度
     */
    private int width;

    /**
     * 高度
     */
    private int height;

    /**
     * uri
     */
    private String uri;

    /**
     * 照片信息
     */
    public PhotoInfo() {
    }

    /**
     * 照片信息
     *
     * @param in 在
     */
    protected PhotoInfo(Parcel in) {
        this.photoId = in.readString();
        this.originalUrl = in.readString();
        this.thumbnailUrl = in.readString();
        this.width = in.readInt();
        this.height = in.readInt();
    }

    /**
     * 有照片的身份证
     *
     * @return {@link String}
     */
    public String getPhotoId() {
        return photoId;
    }

    /**
     * 设置照片的身份证
     *
     * @param photoId 照片的身份证
     */
    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    /**
     * 原始url设置
     *
     * @param originalUrl 原始url
     */
    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    /**
     * 得到缩略图url
     *
     * @return {@link String}
     */
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    /**
     * 设置缩略图url
     *
     * @param thumbnailUrl 缩略图url
     */
    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    /**
     * 得到宽度
     *
     * @return int
     */
    public int getWidth() {
        return width;
    }

    /**
     * 设置宽度
     *
     * @param width 宽度
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * 得到高度
     *
     * @return int
     */
    public int getHeight() {
        return height;
    }

    /**
     * 设置高度
     *
     * @param height 高度
     */
    public void setHeight(int height) {
        this.height = height;
    }
    /**
     * 得到高度
     *
     * @return uri
     */
    public String getUri() {
        return uri;
    }

    /**
     * 设置uri
     *
     * @param uri
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public String toString() {
        return "PhotoInfo{"
            + "photoId='" + photoId + '\''
            + ", originalUrl='" + originalUrl + '\''
            + ", thumbnailUrl='" + thumbnailUrl + '\''
            + ", width=" + width
            + ", height=" + height + '}';
    }
}
