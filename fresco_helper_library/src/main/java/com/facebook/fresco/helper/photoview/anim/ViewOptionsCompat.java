/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.photoview.anim;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.Rect;
import ohos.agp.window.service.WindowManager;

import com.facebook.fresco.helper.utils.PhotoConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * 视图选择兼容
 *
 * @author dev
 * @since 2021-08-02
 */
public class ViewOptionsCompat {
    /**
     * 主要查看选项列表
     */
    public static final String KEY_VIEW_OPTION_LIST = "view_option_list";

    private ViewOptionsCompat() {
    }

    /**
     * 使扩大动画
     *
     * @param thumbnailList 缩略图列表
     * @param listContainer 列表容器
     * @return {@link Intent}
     */
    public static Intent makeScaleUpAnimation(ListContainer listContainer, List<String> thumbnailList) {
        ArrayList<ViewOptions> sparseArray = new ArrayList<>();
        for (int i = 0; i < thumbnailList.size(); i++) {
            Component childView = listContainer.getComponentAt(i);
            if (childView != null) {
                sparseArray.add(createViewOptions(childView, thumbnailList.get(i)));
            }
        }
        Intent intent = new Intent();
        intent.setParam(KEY_VIEW_OPTION_LIST, sparseArray);
        return intent;
    }

    /**
     * 使扩大动画
     *
     * @param thumbnailView 缩略图视图
     * @param thumbnail 缩略图
     * @return {@link Intent}
     */
    public static Intent makeScaleUpAnimation(Component thumbnailView, String thumbnail) {
        ArrayList<ViewOptions> sparseArray = new ArrayList<>();
        sparseArray.add(0, createViewOptions(thumbnailView, thumbnail));

        Intent bundle = new Intent();
        bundle.setParam(KEY_VIEW_OPTION_LIST, sparseArray);
        return bundle;
    }

    /**
     * 创建视图选项
     *
     * @param thumbnailView 缩略图视图
     * @param thumbnail 缩略图
     * @return {@link ViewOptions}
     */
    private static ViewOptions createViewOptions(Component thumbnailView, String thumbnail) {
        ViewOptions viewOptions = new ViewOptions();
        viewOptions.setThumbnail(thumbnail);

        // 判断当前activity是否是全屏模式
        viewOptions.setFullScreen(isFullScreen((Ability) thumbnailView.getContext()));
        // 判断当前是否是竖屏
        viewOptions.setVerticalScreen(isVerticalScreen((Ability) thumbnailView.getContext()));
        // 判断view是否在屏幕上，如果在就执行动画，否则不执行动画
        viewOptions.setInTheScreen(isInScreen(thumbnailView));

        int[] location;
        // ps = position，目的得到当前view相对于屏幕的坐标
        location = thumbnailView.getLocationOnScreen();
        // 设置起始坐标和起始宽高
        viewOptions.setStartX(location[0]);
        viewOptions.setStartY(location[1]);

        viewOptions.setWidth(thumbnailView.getEstimatedWidth());
        viewOptions.setHeight(thumbnailView.getEstimatedHeight());
        return viewOptions;
    }

    /**
     * view当前是否显示在屏幕上，包括被遮挡，显示不全的状态
     *
     * @param view 视图
     * @return boolean
     */
    public static boolean isInScreen(Component view) {
        Rect bounds = new Rect();
        // 只要有一部分显示在屏幕内，就是true，不考虑遮挡情况
        boolean isInScreen = view.getSelfVisibleRect(bounds);
        if (isInScreen) {
            if (bounds.getWidth() < view.getWidth() * 0.3f || bounds.getHeight() < view.getHeight() * 0.3f) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }


    /**
     * 判断当前屏幕是否是横屏
     *
     * @param ability 活动
     * @return boolean
     */
    public static boolean isVerticalScreen(Ability ability) {
        int flag = ability.getAbilityInfo().getOrientation().ordinal();
        return !(flag == 1);
    }

    /**
     * 是否为全屏
     *
     * @param activity 活动
     * @return boolean 判断当前手机是否是全屏
     */
    public static boolean isFullScreen(Ability activity) {
        int flag = activity.getWindow().getLayoutConfig().get().flags;
        if ((flag & WindowManager.LayoutConfig.MARK_FULL_SCREEN)
            == WindowManager.LayoutConfig.MARK_FULL_SCREEN) {
            return true;
        } else {
            return false;
        }
    }
}
