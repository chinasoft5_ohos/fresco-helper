/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.photoview.photodraweeview;

import ohos.multimodalinput.event.TouchEvent;

import com.alexvasilkov.gestures.GestureDetector;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.DraweeView;
import com.oszc.bbhmlibrary.wrapper.RectF;

/**
 * 双击侦听器
 *
 * @author dev
 * @since 2021-08-03
 */
public class DefaultOnDoubleTapListener implements GestureDetector.OnDoubleTapListener {

    private Attacher mAttacher;

    /**
     * 违约双击侦听器
     *
     * @param attacher 连接器
     */
    public DefaultOnDoubleTapListener(Attacher attacher) {
        setPhotoDraweeViewAttacher(attacher);
    }

    /**
     * 设置照片付款人视图连接器
     *
     * @param attacher 连接器
     */
    public void setPhotoDraweeViewAttacher(Attacher attacher) {
        mAttacher = attacher;
    }

    @Override
    public boolean onSingleTapConfirmed(TouchEvent event) {
        if (mAttacher == null) {
            return false;
        }
        DraweeView<GenericDraweeHierarchy> draweeView = mAttacher.getDraweeView();
        if (draweeView == null) {
            return false;
        }
        if (mAttacher.getOnPhotoTapListener() != null) {
            final RectF displayRect = mAttacher.getDisplayRect();
            if (null != displayRect) {
                float x = event.getPointerScreenPosition(0).getX();
                float y = event.getPointerScreenPosition(0).getY();
                if (displayRect.isInclude(x, y)) {
                    float resultX = (x - displayRect.left) / displayRect.getWidth();
                    float resultY = (y - displayRect.top) / displayRect.getHeight();
                    mAttacher.getOnPhotoTapListener().onPhotoTap(draweeView, resultX, resultY);
                    return true;
                }
            }
        }
        if (mAttacher.getOnViewTapListener() != null) {
            mAttacher.getOnViewTapListener().onViewTap(draweeView,
                event.getPointerScreenPosition(0).getX(), event.getPointerScreenPosition(0).getY());
            return true;
        }

        return false;
    }

    @Override
    public boolean onDoubleTap(TouchEvent event) {
        if (mAttacher == null) {
            return false;
        }
        float scale = mAttacher.getScale2();
        float x1 = event.getPointerScreenPosition(0).getX();
        float y1 = event.getPointerScreenPosition(0).getY();

        if (scale < mAttacher.getMediumScale()) {
            mAttacher.setScale(mAttacher.getMediumScale(), x1, y1, true);
        } else if (scale >= mAttacher.getMediumScale() && scale < mAttacher.getMaximumScale()) {
            mAttacher.setScale(mAttacher.getMaximumScale(), x1, y1, true);
        } else {
            mAttacher.setScale(mAttacher.getMinimumScale(), x1, y1, true);
        }
        return false;
    }

    @Override
    public boolean onDoubleTapEvent(TouchEvent event) {
        return false;
    }
}
