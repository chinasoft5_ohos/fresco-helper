/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.utils;

import ohos.agp.components.AttrHelper;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.configuration.DeviceCapability;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.solidxml.TypedAttribute;

import java.util.Optional;

/**
 * 密度工具类
 *
 * @author dev
 * @since 2021-07-29
 */
public class DensityUtil {
    private DensityUtil() {
    }

    /**
     * 得到显示高度
     *
     * @param context 上下文
     * @return int
     */
    public static int getDisplayHeight(Context context) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        Point pt = new Point();
        display.get().getSize(pt);
        return display.get().getAttributes().height;
    }

    /**
     * 得到显示宽度
     *
     * @param context 上下文
     * @return int
     */
    public static int getDisplayWidth(Context context) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        Point pt = new Point();
        display.get().getSize(pt);
        return display.get().getAttributes().width;
    }

    /**
     * 跌至像素
     *
     * @param context 上下文
     * @param dip 浸
     * @return int
     */
    public static int vpToPixels(Context context, float dip) {
        return vp2px(context,dip);
    }

    /**
     * dip2px
     *
     * @param var0 var0
     * @param var1 var1
     * @return int
     */
    public static int vp2px(Context var0, float var1) {
        return AttrHelper.vp2px(var1, var0);
    }

    /**
     * 目前Ohos不支持这个转换,需要自己写
     * px2dip
     *
     * @param var0 var0
     * @param var1 var1
     * @return int
     */
    public static int px2vp(Context var0, float var1) {
        float var2 = var0.getResourceManager().getDeviceCapability().screenDensity;
        return (int) (var1 / var2 + 0.5F);
    }

    /**
     * fp2px
     *
     * @param var0 var0
     * @param var1 var1
     * @return int
     */
    public static int fp2px(Context var0, float var1) {
        return AttrHelper.fp2px(var1, var0);
    }

    /**
     * 目前Ohos不支持这个转换,需要自己写
     * px2fp
     *
     * @param var0 var0
     * @param var1 var1
     * @return int
     */
    public static int px2fp(Context var0, float var1) {
        float var2 = var0.getResourceManager().getDeviceCapability().screenDensity;
        return (int) (var1 / var2 + 0.5F);
    }
}
