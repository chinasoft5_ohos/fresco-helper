/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.statusbar;

import ohos.aafwk.ability.Ability;

/**
 * 状态栏兼容
 *
 * @author dev
 * @since 2021-08-03
 */
public class StatusBarCompat {
    private StatusBarCompat() {
    }

    /**
     * 计算状态栏颜色
     *
     * @param color 颜色
     * @param alpha α
     * @return int
     */
    static int calculateStatusBarColor(int color, int alpha) {
        float a = 1 - alpha / 255f;
        int red = color >> 16 & 0xff;
        int green = color >> 8 & 0xff;
        int blue = color & 0xff;
        red = (int) (red * a + 0.5);
        green = (int) (green * a + 0.5);
        blue = (int) (blue * a + 0.5);
        return 0xff << 24 | red << 16 | green << 8 | blue;
    }

    /**
     * 设置状态栏颜色
     *
     * @param ability 活动
     * @param statusColor 状态的颜色
     * @param alpha alpha
     */
    public static void setStatusBarColor(Ability ability, int statusColor, int alpha) {
        setStatusBarColor(ability, calculateStatusBarColor(statusColor, alpha));
    }

    /**
     * 设置状态栏颜色
     *
     * @param ability 能力
     * @param statusColor 状态的颜色
     */
    public static void setStatusBarColor(Ability ability, int statusColor) {
        StatusBarCompatLollipop.setStatusBarColor(ability, statusColor);

    }


    /**
     * 透明状态栏
     *
     * @param activity 活动
     */
    public static void translucentStatusBar(Ability activity) {
        translucentStatusBar(activity, false);
    }

    /**
     * 透明状态栏
     *
     * @param activity 活动
     * @param hideStatusBarBackground 隐藏状态栏背景
     */
    public static void translucentStatusBar(Ability activity, boolean hideStatusBarBackground) {
        StatusBarCompatLollipop.translucentStatusBar(activity, hideStatusBarBackground);

    }
}
