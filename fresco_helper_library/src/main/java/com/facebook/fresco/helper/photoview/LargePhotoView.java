/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.photoview;

import ohos.agp.components.AttrSet;
import ohos.app.Context;

import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.facebook.fresco.helper.listener.OnProgressListener;
import com.facebook.fresco.helper.utils.MLog;

/**
 * 大照片视图
 *
 * @author dev
 * @since 2021-08-03
 */
public class LargePhotoView extends SubsamplingScaleImageView {
    private OnProgressListener mOnProgressListener;

    /**
     * 大照片视图
     *
     * @param context 上下文
     */
    public LargePhotoView(Context context) {
        super(context);
    }

    /**
     * 大照片视图
     *
     * @param context 上下文
     * @param attrSet attr集
     */
    public LargePhotoView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    /**
     * 设置进度监听器
     *
     * @param listener 侦听器
     */
    public void setOnProgressListener(OnProgressListener listener) {
        this.mOnProgressListener = listener;
    }

    /**
     * 加载进度
     *
     * @param progress 0~100
     */
    public void onProgress(int progress) {
        MLog.info("progress = " + progress);
        if (mOnProgressListener != null) {
            mOnProgressListener.onProgress(progress);
        }
    }
}
