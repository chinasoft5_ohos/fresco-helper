/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.blur;

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.app.Context;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

/**
 * 对Bitmap进行高斯模糊处理
 *
 * @since 2021-07-29
 */
public class BitmapBlurHelper {
    private static final int RADIUS_CONSTANT = 25;
    private static final int SAMPLING_CONSTANT = 1;

    private BitmapBlurHelper() {
    }

    /**
     * 对Bitmap进行高斯模糊处理
     *
     * @param context Context
     * @param source PixelMap
     * @return PixelMap
     */
    public static PixelMap blur(Context context, PixelMap source) {
        int sampling = SAMPLING_CONSTANT;
        int radius = RADIUS_CONSTANT;

        int width = source.getImageInfo().size.width;
        int height = source.getImageInfo().size.height;
        int scaledWidth = width / sampling;
        int scaledHeight = height / sampling;

        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = source.getImageInfo().size;
        options.pixelFormat = PixelFormat.ARGB_8888;
        options.size.width = scaledWidth;
        options.size.height = scaledHeight;
        PixelMap blurredBitmap = PixelMap.create(source, options);

        Canvas canvas = new Canvas();
        canvas.scale(1.0F / (float) sampling, 1.0F / (float) sampling);
        Paint paint = new Paint();
        PixelMapHolder holder = new PixelMapHolder(source);
        canvas.drawPixelMapHolder(holder, 0.0F, 0.0F, paint);

        blurredBitmap = FastBlur.blur(blurredBitmap, radius, true);

        assert blurredBitmap != null;
        Rect rect = new Rect();
        rect.width = width;
        rect.height = height;
        PixelMap scaledBitmap = PixelMap.create(blurredBitmap, rect, new PixelMap.InitializationOptions());
        blurredBitmap.release();
        return scaledBitmap;
    }

    /**
     * 做高斯模糊处理
     *
     * @param source Bitmap
     * @param radius 值越大越模糊，取值范围1~100
     */
    public static void blur(PixelMap source, int radius) {
        FastBlur.blur(source, radius, true);
    }

    /**
     * blur2
     *
     * @param source 源
     * @param radius 半径
     * @return {@link PixelMap}
     */
    public static PixelMap blur2(PixelMap source, int radius) {
        return FastBlur.blur(source, radius, true);
    }

    /**
     * 得到的不透明度
     *
     * @return int
     */
    public int getOpacity() {
        return PixelFormat.UNKNOWN.getValue();
    }
}
