/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.loading;

import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.LinearShader;
import ohos.agp.render.Paint;
import ohos.agp.render.Shader;
import ohos.agp.render.SweepShader;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import com.oszc.bbhmlibrary.wrapper.RectF;

/**
 * 进度条元素
 *
 * @author dev
 * @since 2021-08-02
 */
public class ProgressBarElement extends PixelMapElement {
    /**
     * m最大价值
     */
    protected long mMaxValue = 10000;
    /**
     * 进展
     */
    protected long mProgress;
    /**
     * 文本大小
     */
    private int mTextSize;
    /**
     * 文本颜色
     */
    private int mTextColor;
    /**
     * 文本xoffset
     */
    private int mTextXOffset;
    /**
     * 文本偏移量
     */
    private int mTextYOffset;
    /**
     * 字体
     */
    private Font mTypeface;
    /**
     * m文本显示
     */
    private boolean mTextShow;
    /**
     * 文本画笔
     */
    private Paint mTextPaint;
    /**
     * 定制str
     */
    private String mCustomStr;
    /**
     * 圆画笔
     */
    private Paint mCirclePaint;
    /**
     * 圆宽度
     */
    private float mCircleWidth;
    /**
     * 圆进度颜色
     */
    private int mCircleProgressColor;
    /**
     * 圆底的颜色
     */
    private int mCircleBottomColor;
    /**
     * 圆底宽
     */
    private int mCircleBottomWidth;
    /**
     * 圆半径
     */
    private int mCircleRadius;
    /**
     * 填充
     */
    private int mFanPadding;
    /**
     * 圆风格
     */
    private CircleStyle mCircleStyle = CircleStyle.FAN;
    /**
     * 渐变颜色
     */
    private int[] mGradientColor;
    /**
     * 梯度类型
     */
    private GradientType mGradientType = GradientType.LINEAR;

    /**
     * 进度条元素
     *
     * @param pixelMap 像素映射
     */
    public ProgressBarElement(PixelMap pixelMap) {
        super(pixelMap);
        initProperty();
    }

    @Override
    public void drawToCanvas(Canvas canvas) {
        super.drawToCanvas(canvas);
        if (mProgress == 0 || (int) ((double) ((double) mProgress / (double) mMaxValue) * 100) == 100) {
            return;
        }
        DrawOther(canvas);
        if (mTextShow) {
            DrawText(canvas);
        }
    }

    /**
     * 绘制文本
     *
     * @param canvas 帆布
     */
    public void DrawText(Canvas canvas) {
        Rect size = getBounds();
        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
        int baseline = (int) (size.top + (size.bottom - size.top - fontMetrics.bottom + fontMetrics.top) / 2 - fontMetrics.top);
        canvas.drawText(mTextPaint, mCustomStr == null ? (int) (((double) mProgress / (double) mMaxValue) * 100) + "%" : mCustomStr,
            size.getCenterX() + mTextXOffset, baseline + mTextYOffset);
    }

    /**
     * 水平变化
     *
     * @param level 水平
     * @return boolean
     */
    protected boolean onLevelChange(int level) {
        long origin = mProgress;
        mProgress = level;
        if (mProgress != 0 && origin != mProgress) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 设置文本大小
     *
     * @param textSize m文本大小
     */
    public void setTextSize(int textSize) {
        this.mTextSize = textSize;
        mTextPaint.setTextSize(mTextSize);
    }

    /**
     * 设置文本颜色
     *
     * @param textColor m文本颜色
     */
    public void setTextColor(int textColor) {
        mTextPaint.setColor(new Color(textColor));
        this.mTextColor = textColor;
    }


    /**
     * 设置文本颜色res
     *
     * @param colorRes 颜色res
     * @param context 上下文
     */
    public void setTextColorRes(int colorRes, Context context) {
        mTextPaint.setColor(new Color(context.getColor(colorRes)));
    }

    /**
     * 设置文本显示
     *
     * @param textShow 文本显示
     */
    public void setTextShow(boolean textShow) {
        this.mTextShow = textShow;
    }

    /**
     * 设置字体
     *
     * @param typeface 字体
     */
    public void setTypeface(Font typeface) {
        this.mTypeface = typeface;
        mTextPaint.setFont(mTypeface);
    }

    /**
     * 设置自定义文本
     *
     * @param text 文本
     */
    public void setCustomText(String text) {
        mCustomStr = text;
    }

    /**
     * 设置文本xoffset
     *
     * @param offset 抵消
     */
    public void setTextXOffset(int offset) {
        mTextXOffset = offset;
    }

    /**
     * 设置文本yoffset
     *
     * @param offset 抵消
     */
    public void setTextYOffset(int offset) {
        mTextYOffset = offset;
    }

    /**
     * 设置最大价值
     *
     * @param value 价值
     */
    public void setMaxValue(long value) {
        this.mMaxValue = value;
    }

    @Override
    public void setAlpha(int alpha) {
    }

    /**
     * getm文本油漆
     *
     * @return {@link Paint}
     */
    public Paint getTextPaint() {
        return mTextPaint;
    }

    private void initProperty() {
        mTextPaint = new Paint();
        mTextSize = 20;
        mTextColor = Color.WHITE.getValue();
        mTextShow = true;
        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setColor(new Color(mTextColor));
        mTextPaint.setTextSize(mTextSize);
        mTextPaint.setTextAlign(TextAlignment.CENTER);

        mCirclePaint = new Paint();

        mCirclePaint.setAntiAlias(true);
        mCirclePaint.setStrokeWidth(10);
        mCirclePaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);

        mCircleProgressColor = 0xaa59c8cc;
        mCircleBottomColor = 0xdddddddd;
        mCircleWidth = 8;
        mCircleRadius = 100;
        mFanPadding = 5;
        mCircleBottomWidth = 2;

    }

    /**
     * 吸引其他
     *
     * @param canvas 帆布
     */
    public void DrawOther(Canvas canvas) {
        drawCircle(canvas);
        drawArc(canvas);
    }

    /**
     * 画圆
     *
     * @param canvas 帆布
     */
    private void drawCircle(Canvas canvas) {

        Rect bounds = getBounds();
        int xpos = bounds.left + bounds.getWidth() / 2;
        int ypos = bounds.bottom - bounds.getHeight() / 2;

        mCirclePaint.setColor(new Color(mCircleBottomColor));
        mCirclePaint.setStrokeWidth(mCircleStyle == CircleStyle.RING ? mCircleWidth : mCircleBottomWidth);
        mCirclePaint.setStyle(Paint.Style.STROKE_STYLE);
        mCirclePaint.setShader(null, Paint.ShaderType.RADIAL_SHADER);
        canvas.drawCircle(xpos, ypos, mCircleStyle == CircleStyle.RING
            ? mCircleRadius : mCircleRadius + mFanPadding, mCirclePaint);

    }

    /**
     * 画弧
     *
     * @param canvas 帆布
     */
    private void drawArc(Canvas canvas) {
        mCirclePaint.setStyle(mCircleStyle == CircleStyle.RING
            ? Paint.Style.STROKE_STYLE : Paint.Style.FILL_STYLE);
        Rect bounds = getBounds();
        int xpos = bounds.left + bounds.getWidth() / 2;
        int ypos = bounds.bottom - bounds.getHeight() / 2;
        RectF rectF = new RectF();
        rectF.set(xpos - mCircleRadius, ypos - mCircleRadius, xpos + mCircleRadius, ypos + mCircleRadius);
        float degree = (float) mProgress / (float) mMaxValue * 360;
        mCirclePaint.setStrokeWidth(mCircleWidth);
        if (mGradientColor != null) {
            if (mGradientType == GradientType.LINEAR) {
                Point[] points = new Point[2];
                points[0] = new Point(bounds.getCenterX(), bounds.getCenterY() - mCircleRadius);
                points[1] = new Point(bounds.getCenterX(), bounds.getCenterY() + mCircleRadius);
                float[] floats = new float[mGradientColor.length];
                for (int i1 = 0; i1 < mGradientColor.length; i1++) {
                    floats[i1] = mGradientColor[i1];
                }
                mCirclePaint.setShader(new LinearShader(points, floats, null, Shader.TileMode.MIRROR_TILEMODE), Paint.ShaderType.LINEAR_SHADER);
            } else {
                Color[] colors = new Color[mGradientColor.length];
                for (int ii = 0; ii < mGradientColor.length; ii++) {
                    colors[ii] = new Color(mGradientColor[ii]);
                }
                mCirclePaint.setShader(new SweepShader(bounds.getCenterX(), bounds.getCenterY(), colors, null), Paint.ShaderType.SWEEP_SHADER);
            }
            mCirclePaint.setColor(new Color(Color.getIntColor("#ffffff")));
        } else {
            mCirclePaint.setColor(new Color(mCircleProgressColor));
        }
        canvas.drawArc(rectF, new Arc(270, degree, mCircleStyle == CircleStyle.FAN), mCirclePaint);
    }

    /**
     * 圆的风格
     *
     * @author dev
     * @since 2021-08-02
     */
    public enum CircleStyle {
        /**
         * 环
         */
        RING,
        /**
         * 风扇
         */
        FAN
    }

    /**
     * 梯度类型
     *
     * @author dev
     * @since 2021-08-02
     */
    public enum GradientType {
        /**
         * 线性
         */
        LINEAR,
        /**
         * 扫描
         */
        SWEEP;
    }
}
