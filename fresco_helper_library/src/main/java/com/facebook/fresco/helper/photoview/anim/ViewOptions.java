/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.photoview.anim;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

import java.io.Serializable;

/**
 * 视图选项
 *
 * @author dev
 * @since 2021-08-02
 */
public class ViewOptions implements Serializable {
    private String thumbnail;
    private int startX;
    private int startY;
    private int width;
    private int height;
    /**
     * 当前是否是全屏
     */
    private boolean isFullScreen;
    /**
     * 当前是否是竖屏
     */
    private boolean isVerticalScreen;
    /**
     * 当前view是否在屏幕上
     */
    private boolean isInTheScreen;

    /**
     * 视图选项
     */
    public ViewOptions() {
    }

    /**
     * 得到的缩略图
     *
     * @return {@link String}
     */
    public String getThumbnail() {
        return thumbnail;
    }

    /**
     * 设置缩略图
     *
     * @param thumbnail 缩略图
     */
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    /**
     * 得到startx
     *
     * @return int
     */
    public int getStartX() {
        return startX;
    }

    /**
     * 设置startx
     *
     * @param startX startx
     */
    public void setStartX(int startX) {
        this.startX = startX;
    }

    /**
     * 得到starty
     *
     * @return int
     */
    public int getStartY() {
        return startY;
    }

    /**
     * 设置starty
     *
     * @param startY starty
     */
    public void setStartY(int startY) {
        this.startY = startY;
    }

    /**
     * 得到宽度
     *
     * @return int
     */
    public int getWidth() {
        return width;
    }

    /**
     * 设置宽度
     *
     * @param width 宽度
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * 得到高度
     *
     * @return int
     */
    public int getHeight() {
        return height;
    }

    /**
     * 设置高度
     *
     * @param height 高度
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * 是全屏
     *
     * @return boolean
     */
    public boolean isFullScreen() {
        return isFullScreen;
    }

    /**
     * 设置全屏
     *
     * @param isFull 全屏
     */
    public void setFullScreen(boolean isFull) {
        isFullScreen = isFull;
    }

    /**
     * 是垂直的屏幕
     *
     * @return boolean
     */
    public boolean isVerticalScreen() {
        return isVerticalScreen;
    }

    /**
     * 设置垂直屏幕
     *
     * @param isVertical 垂直屏幕
     */
    public void setVerticalScreen(boolean isVertical) {
        this.isVerticalScreen = isVertical;
    }

    /**
     * 在屏幕上
     *
     * @return boolean
     */
    public boolean isInTheScreen() {
        return isInTheScreen;
    }

    /**
     * 在屏幕上
     *
     * @param isInScreen 在屏幕上
     */
    public void setInTheScreen(boolean isInScreen) {
        isInTheScreen = isInScreen;
    }
}
