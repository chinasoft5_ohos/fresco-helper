/**
 * Copyright (C) 2015 Wasabeef
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.blur;

import ohos.media.image.PixelMap;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

/**
 * 快速模糊
 *
 * @author dev
 * @since 2021-07-29
 */
public class FastBlur {

    /**
     * 模糊
     *
     * @param sentBitmap 把位图
     * @param radius 半径
     * @param canReuseInBitmap 可以重用的位图
     * @return {@link PixelMap}
     */
    public static PixelMap blur(PixelMap sentBitmap, int radius, boolean canReuseInBitmap) {
        PixelMap bitmap;
        if (canReuseInBitmap) {
            bitmap = sentBitmap;
        } else {
            bitmap = PixelMap.create(sentBitmap, new PixelMap.InitializationOptions());
        }

        if (radius < 1) {
            return (null);
        }

        int w1 = bitmap.getImageInfo().size.width;
        int h2 = bitmap.getImageInfo().size.height;

        Rect rect = new Rect(0, 0, w1, h2);
        int[] pix = new int[w1 * h2];
        bitmap.readPixels(pix, 0, w1, rect);

        int wm = w1 - 1;
        int hm = h2 - 1;
        int wh = w1 * h2;
        int div = radius + radius + 1;

        int red[] = new int[wh];
        int green[] = new int[wh];
        int blue[] = new int[wh];
        int rsum, gsum, bsum, x1, y1, i1, p1, yp, yi, yw;
        int vmin[] = new int[Math.max(w1, h2)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i1 = 0; i1 < 256 * divsum; i1++) {
            dv[i1] = (i1 / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y1 = 0; y1 < h2; y1++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i1 = -radius; i1 <= radius; i1++) {
                p1 = pix[yi + Math.min(wm, Math.max(i1, 0))];
                sir = stack[i1 + radius];
                sir[0] = (p1 & 0xff0000) >> 16;
                sir[1] = (p1 & 0x00ff00) >> 8;
                sir[2] = (p1 & 0x0000ff);
                rbs = r1 - Math.abs(i1);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i1 > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x1 = 0; x1 < w1; x1++) {

                red[yi] = dv[rsum];
                green[yi] = dv[gsum];
                blue[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y1 == 0) {
                    vmin[x1] = Math.min(x1 + radius + 1, wm);
                }
                p1 = pix[yw + vmin[x1]];

                sir[0] = (p1 & 0xff0000) >> 16;
                sir[1] = (p1 & 0x00ff00) >> 8;
                sir[2] = (p1 & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w1;
        }
        for (x1 = 0; x1 < w1; x1++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w1;
            for (i1 = -radius; i1 <= radius; i1++) {
                yi = Math.max(0, yp) + x1;

                sir = stack[i1 + radius];

                sir[0] = red[yi];
                sir[1] = green[yi];
                sir[2] = blue[yi];

                rbs = r1 - Math.abs(i1);

                rsum += red[yi] * rbs;
                gsum += green[yi] * rbs;
                bsum += blue[yi] * rbs;

                if (i1 > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i1 < hm) {
                    yp += w1;
                }
            }
            yi = x1;
            stackpointer = radius;
            for (y1 = 0; y1 < h2; y1++) {
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x1 == 0) {
                    vmin[y1] = Math.min(y1 + r1, hm) * w1;
                }
                p1 = x1 + vmin[y1];

                sir[0] = red[p1];
                sir[1] = green[p1];
                sir[2] = blue[p1];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w1;
            }
        }

        final PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(w1, h2);

        return PixelMap.create(pix, 0, w1, initializationOptions);
    }
}
