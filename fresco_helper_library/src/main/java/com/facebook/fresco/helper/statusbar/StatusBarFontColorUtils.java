/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.statusbar;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;

import com.facebook.fresco.helper.utils.MLog;
import com.oszc.bbhmlibrary.wrapper.TextUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

/**
 * 状态栏字体颜色
 *
 * @author dev
 * @since 2021-08-04
 */
public class StatusBarFontColorUtils {
    private static final String ODM_PRODUCT = "ODM_PRODUCT";

    private StatusBarFontColorUtils() {
    }

    /**
     * 状态栏光模式
     *
     * @param ability 活动
     * @param isBlack 黑色的
     */
    public static void statusBarLightMode(Ability ability, boolean isBlack) {
        if (isMeizu()) {
            flymeSetStatusBarLightMode(ability.getWindow(), isBlack);
        } else if (isXiaomi()) {
            miuiSetStatusBarLightMode(ability.getWindow(), isBlack);
        }

        if (isBlack) {
            ability.getWindow().setStatusBarVisibility(WindowManager.LayoutConfig.SYSTEM_BAR_BRIGHT_STATUS);
        } else {
            ability.getWindow().setStatusBarVisibility(WindowManager.LayoutConfig.INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    /**
     * flyme设置状态栏光模式
     *
     * @param window 窗口
     * @param isDark 黑暗
     */
    public static void flymeSetStatusBarLightMode(Window window, boolean isDark) {
        if (window != null) {
            try {
                WindowManager.LayoutConfig lp = window.getLayoutConfig().get();
                Field darkFlag = WindowManager.LayoutConfig.class
                    .getDeclaredField("MEIZU_FLAG_DARK_STATUS_BAR_ICON");
                Field meizuFlags = WindowManager.LayoutConfig.class
                    .getDeclaredField("meizuFlags");
                darkFlag.setAccessible(true);
                meizuFlags.setAccessible(true);
                int bit = darkFlag.getInt(null);
                int value = meizuFlags.getInt(lp);
                if (isDark) {
                    value |= bit;
                } else {
                    value &= ~bit;
                }
                meizuFlags.setInt(lp, value);
                window.setLayoutConfig(lp);
            } catch (NoSuchFieldException error) {
                MLog.error(" FlymeSetStatusBarLightMode NoSuchFieldException " + error.getLocalizedMessage());
            } catch (IllegalAccessException error) {
                MLog.error(" FlymeSetStatusBarLightMode IllegalAccessException " + error.getLocalizedMessage());
            }
        }
    }

    /**
     * miuiset状态栏光模式
     *
     * @param window 窗口
     * @param isDark 黑暗
     */
    public static void miuiSetStatusBarLightMode(Window window, boolean isDark) {
        if (window != null) {
            Class clazz = window.getClass();
            try {
                int darkModeFlag = 0;
                Class layoutParams = Class.forName("ohos.component.MiuiWindowManager$LayoutConfig");
                Field field = layoutParams.getField("EXTRA_FLAG_STATUS_BAR_DARK_MODE");
                darkModeFlag = field.getInt(layoutParams);
                Method extraFlagField = clazz.getMethod("setExtraFlags", int.class, int.class);
                if (isDark) {
                    extraFlagField.invoke(window, darkModeFlag, darkModeFlag); // 状态栏透明且黑色字体
                } else {
                    extraFlagField.invoke(window, 0, darkModeFlag); // 清除黑色字体
                }
            } catch (InvocationTargetException | IllegalAccessException
                | NoSuchMethodException | ClassNotFoundException error) {
                MLog.error(" MIUISetStatusBarLightMode " + error.getLocalizedMessage());
            } catch (NoSuchFieldException error) {
                MLog.error(" MIUISetStatusBarLightMode NoSuchFieldException " + error.getLocalizedMessage());
            }
        }
    }

    /**
     * 是小米
     * 检测是否Xiaomi手机
     *
     * @return boolean
     */
    public static boolean isXiaomi() {
        String manufacturer = System.getenv(ODM_PRODUCT);
        return TextUtils.equals("xiaomi", manufacturer.toLowerCase(Locale.ROOT));
    }

    /**
     * 是魅族
     * 检测是否Meizu手机
     *
     * @return boolean
     */
    public static boolean isMeizu() {
        String manufacturer = System.getenv(ODM_PRODUCT);
        return TextUtils.equals("meizu", manufacturer.toLowerCase(Locale.ROOT));
    }

    /**
     * 是朋友
     * 检测是否Oppo手机
     *
     * @return boolean
     */
    public static boolean isOppo() {
        String manufacturer = System.getenv(ODM_PRODUCT);
        return TextUtils.equals("oppo", manufacturer.toLowerCase(Locale.ROOT));
    }

    /**
     * 检测是否HUAWEI手机
     *
     * @return boolean
     */
    public static boolean isHuawei() {
        String manufacturer = System.getenv(ODM_PRODUCT);
        return TextUtils.equals("huawei", manufacturer.toLowerCase(Locale.ROOT));
    }
}
