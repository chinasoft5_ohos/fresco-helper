/**
 * ****************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * *****************************************************************************
 */

package com.facebook.fresco.helper.photoview.photodraweeview;

/**
 * 在规模拖动手势侦听器
 *
 * @author dev
 * @since 2021-08-03
 */
public interface OnScaleDragGestureListener {
    /**
     * 在拖
     *
     * @param dx dx
     * @param dy dy
     */
    void onDrag(float dx, float dy);

    /**
     * 在舞
     *
     * @param startX startx
     * @param startY starty
     * @param velocityX velocityx
     * @param velocityY velocityy
     */
    void onFling(float startX, float startY, float velocityX, float velocityY);

    /**
     * 在规模
     *
     * @param scaleFactor 比例因子
     * @param focusX focusx
     * @param focusY focusy
     */
    void onScale(float scaleFactor, float focusX, float focusY);

    /**
     * 在规模
     */
    void onScaleEnd();
}
