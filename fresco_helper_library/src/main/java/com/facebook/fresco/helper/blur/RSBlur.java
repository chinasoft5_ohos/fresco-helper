/**
 * Copyright (C) 2015 Wasabeef
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.blur;

import ohos.app.Context;
import ohos.media.image.PixelMap;

/**
 * 目前Ohos不支持该图片处理框架
 * rsblur
 *
 * @author dev
 * @since 2021-07-29
 */
public class RSBlur {

    /**
     * 模糊
     *
     * @param context 上下文
     * @param bitmap 位图
     * @param radius 半径
     * @return {@link PixelMap}
     * @throws RuntimeException 运行时异常
     */
    public static PixelMap blur(Context context, PixelMap bitmap, int radius) throws RuntimeException {
//        RenderScript rs = null;
//        try {
//            rs = RenderScript.create(context); // ohos无此api
//            Allocation input =
//                Allocation.createFromBitmap(rs, bitmap, Allocation.MipmapControl.MIPMAP_NONE,
//                    Allocation.USAGE_SCRIPT);
//            Allocation output = Allocation.createTyped(rs, input.getType());
//            ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
//
//            blur.setInput(input);
//            blur.setRadius(radius);
//            blur.forEach(output);
//            output.copyTo(bitmap);
//        } finally {
//            if (rs != null) {
//                rs.destroy();
//            }
//        }

        return bitmap;
    }
}
