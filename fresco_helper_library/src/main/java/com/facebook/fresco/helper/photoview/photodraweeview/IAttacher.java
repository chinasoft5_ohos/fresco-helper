/**
 * ****************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * *****************************************************************************
 */

package com.facebook.fresco.helper.photoview.photodraweeview;

import ohos.agp.components.Component;

import com.alexvasilkov.gestures.GestureDetector;

/**
 * iattacher
 *
 * @author dev
 * @since 2021-08-03
 */
public interface IAttacher {
    /**
     * 默认的最大尺度
     */
    public static final float DEFAULT_MAX_SCALE = 3.0f;
    /**
     * 默认的中期规模
     */
    public static final float DEFAULT_MID_SCALE = 1.75f;
    /**
     * 默认的最小规模
     */
    public static final float DEFAULT_MIN_SCALE = 1.0f;
    /**
     * 变焦持续时间
     */
    public static final long ZOOM_DURATION = 200L;

    /**
     * 得到最小规模
     *
     * @return float
     */
    float getMinimumScale();

    /**
     * 得到中等规模
     *
     * @return float
     */
    float getMediumScale();

    /**
     * 得到最大的规模
     *
     * @return float
     */
    float getMaximumScale();

    /**
     * 设置最大尺度
     *
     * @param maximumScale 最大的规模
     */
    void setMaximumScale(float maximumScale);

    /**
     * 设置中等规模
     *
     * @param mediumScale 中等规模
     */
    void setMediumScale(float mediumScale);

    /**
     * 设置最小规模
     *
     * @param minimumScale 最小规模
     */
    void setMinimumScale(float minimumScale);

    /**
     * 获得规模
     *
     * @return float
     */
    float getScale2();

    /**
     * 设置范围
     *
     * @param scale 规模
     */
    void setScale(float scale);

    /**
     * 设置范围
     *
     * @param scale 规模
     * @param isAnimate 动画
     */
    void setScale(float scale, boolean isAnimate);

    /**
     * 设置范围
     *
     * @param scale 规模
     * @param focalX focalx
     * @param focalY 恰城对妇女实施
     * @param isAnimate 动画
     */
    void setScale(float scale, float focalX, float focalY, boolean isAnimate);

    /**
     * 设置放大过渡时间
     *
     * @param duration 持续时间
     */
    void setZoomTransitionDuration(long duration);

    /**
     * 边缘上设置允许父母拦截
     *
     * @param isAllow 允许
     */
    void setAllowParentInterceptOnEdge(boolean isAllow);

    /**
     * 双击侦听器
     *
     * @param listener 侦听器
     */
    void setOnDoubleTapListener(GestureDetector.OnDoubleTapListener listener);

    /**
     * 规模改变监听器
     *
     * @param listener 侦听器
     */
    void setOnScaleChangeListener(OnScaleChangeListener listener);

    /**
     * 长点击监听器
     *
     * @param listener 侦听器
     */
    void setOnLongClickListener(Component.LongClickedListener listener);

    /**
     * 设置在照片利用侦听器
     *
     * @param listener 侦听器
     */
    void setOnPhotoTapListener(OnPhotoTapListener listener);

    /**
     * 利用侦听器上设置视图
     *
     * @param listener 侦听器
     */
    void setOnViewTapListener(OnViewTapListener listener);

    /**
     * 照片点击监听器
     *
     * @return {@link OnPhotoTapListener}
     */
    OnPhotoTapListener getOnPhotoTapListener();

    /**
     * 视图点击监听器
     *
     * @return {@link OnViewTapListener}
     */
    OnViewTapListener getOnViewTapListener();

    /**
     * 更新
     *
     * @param imageInfoWidth 图像信息宽度
     * @param imageInfoHeight 图像信息的高度
     */
    void update(int imageInfoWidth, int imageInfoHeight);
}
