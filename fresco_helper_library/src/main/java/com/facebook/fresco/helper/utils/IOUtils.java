/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.utils;

import ohos.media.image.PixelMap;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 据流处理工具类
 *
 * @author dev
 * @since 2021-08-04
 */
public class IOUtils {
    private static final int CONSTANT = -1;
    private static final int BYTE = 1024;

    private IOUtils() {
    }

    /**
     * 复制
     *
     * @param oldPath 旧的路径
     * @param newPath 新路径
     * @throws IOException ioexception
     */
    public static void copy(String oldPath, String newPath) throws IOException {
        FileInputStream inputStream = new FileInputStream(oldPath);
        FileOutputStream fileOutputStream = new FileOutputStream(newPath);
        byte[] buffer = new byte[BYTE];
        int len;
        while ((len = inputStream.read(buffer)) != CONSTANT) {
            fileOutputStream.write(buffer, 0, len);
        }
        fileOutputStream.flush();
        fileOutputStream.close();
        inputStream.close();
    }

    /**
     * 写
     *
     * @param filePath 文件路径
     * @param data 数据
     * @throws IOException ioexception
     */
    public static void write(String filePath, byte[] data) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(filePath);
        fileOutputStream.write(data);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    /**
     * 读
     *
     * @param path 路径
     * @return {@link byte}
     * @throws IOException ioexception
     */
    public static byte[] read(String path) throws IOException {
        return read(new FileInputStream(path));
    }

    /**
     * 读
     *
     * @param inStream 在流
     * @return {@link byte}
     * @throws IOException ioexception
     */
    public static byte[] read(InputStream inStream) throws IOException {
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[BYTE];
        int len;
        while ((len = inStream.read(buffer)) != CONSTANT) {
            outSteam.write(buffer, 0, len);
        }
        outSteam.flush();
        outSteam.close();
        inStream.close();
        return outSteam.toByteArray();
    }

    /**
     * 读
     *
     * @param bitmap 位图
     * @return {@link byte}
     * @throws IOException ioexception
     */
    public static byte[] read(PixelMap bitmap) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
//        baos.flush();
//        baos.close();
        return baos.toByteArray();
    }

}
