/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.photoview.anim;

import ohos.aafwk.ability.Ability;
import ohos.agp.animation.Animator;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ComponentParent;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;


/**
 * 过渡动画
 *
 * @author dev
 * @since 2021-08-02
 */
public abstract class TransitionAnimator {
    private static final long DURATION = 300;
    /**
     * 米活动
     */
    protected Ability mAbility;
    /**
     * m场景根
     */
    protected Component mSceneRoot;
    private long mDuration = DURATION;
    private int mTimeInterpolator = Animator.CurveType.ACCELERATE_DECELERATE;

    /**
     * 过渡动画
     *
     * @param activity 活动
     */
    public TransitionAnimator(Ability activity, Component component) {
        mAbility = activity;
        mSceneRoot = getRoot(component);
        WindowManager.getInstance().getTopWindow().get().setTransparent(true);
    }

    private ComponentContainer getRoot(Component component) {
        ComponentParent componentParent = component.getComponentParent();
        while (componentParent != null) {
            if (componentParent.getComponentParent() == null) {
                break;
            }
            componentParent = componentParent.getComponentParent();
        }
        return (ComponentContainer) componentParent;
    }

    /**
     * 进入动画结束时应该有的操作
     */
    protected void enterAnimsEnd() {
        mSceneRoot.setAlpha(1.0f);
    }

    /**
     * 退出动画结束时应该有的操作
     */
    protected void exitAnimsEnd() {
        mSceneRoot.setAlpha(0);
        mAbility.terminateAbility();
        mAbility.setTransitionAnimation(0, 0);
    }

    /**
     * 获得能力
     *
     * @return 当前的Ability
     */
    public Ability getAbility() {
        return mAbility;
    }

    /**
     * 得到现场根
     *
     * @return 要进行动画的view
     */
    public Component getSceneRoot() {
        return mSceneRoot;
    }

    /**
     * 得到动画插入器
     *
     * @return int
     */
    public int getAnimInterpolator() {
        return mTimeInterpolator;
    }

    /**
     * 设置动画插入器
     *
     * @param interpolator 插入器
     */
    public void setAnimInterpolator(int interpolator) {
        mTimeInterpolator = interpolator;
    }

    /**
     * 得到动画时间
     *
     * @return long
     */
    public long getAnimDuration() {
        return mDuration;
    }

    /**
     * 设置动画时间
     *
     * @param duration 持续时间
     */
    public void setAnimDuration(long duration) {
        mDuration = duration;
    }
}
