/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.listener;

import ohos.app.Context;

import com.facebook.fresco.helper.utils.ImageFileUtils;

/**
 * 下载图片的结果监听器
 *
 * @since 2021-08-02
 */
public abstract class IDownloadResult implements IResult<String> {
    private String mFilePath;

    /**
     * idownload结果
     *
     * @param filePath 文件路径
     */
    public IDownloadResult(String filePath) {
        this.mFilePath = filePath;
    }

    /**
     * idownload结果
     *
     * @param context 上下文
     */
    public IDownloadResult(Context context) {
        this.mFilePath = ImageFileUtils.getImageDownloadPath(context);
    }

    /**
     * 获取文件路径
     *
     * @return {@link String}
     */
    public String getFilePath() {
        return mFilePath;
    }

    /**
     * 进展
     *
     * @param progress 进步
     */
    public void onProgress(int progress) {
    }

    /**
     * 在结果
     *
     * @param filePath 文件路径
     */
    @Override
    public abstract void onResult(String filePath);
}
