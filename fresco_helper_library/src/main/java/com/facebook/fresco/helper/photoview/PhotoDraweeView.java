/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.photoview;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.app.Context;

import com.alexvasilkov.gestures.GestureDetector;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.fresco.helper.photoview.photodraweeview.Attacher;
import com.facebook.fresco.helper.photoview.photodraweeview.IAttacher;
import com.facebook.fresco.helper.photoview.photodraweeview.OnPhotoTapListener;
import com.facebook.fresco.helper.photoview.photodraweeview.OnScaleChangeListener;
import com.facebook.fresco.helper.photoview.photodraweeview.OnViewTapListener;

import java.util.Optional;

/**
 * 付款人查看照片
 *
 * @author dev
 * @since 2021-08-03
 */
public class PhotoDraweeView extends SimpleDraweeView implements IAttacher, Component.DrawTask {
    private Attacher mAttacher;

    /**
     * 构造参数
     *
     * @param context 上下文
     * @param hierarchy 层次结构
     */
    public PhotoDraweeView(Context context, GenericDraweeHierarchy hierarchy) {
        super(context, hierarchy);
        init();
    }

    /**
     * 构造参数
     *
     * @param context 上下文
     */
    public PhotoDraweeView(Context context) {
        super(context, new AttrSet() {
            @Override
            public Optional<String> getStyle() {
                return Optional.empty();
            }

            @Override
            public int getLength() {
                return 0;
            }

            @Override
            public Optional<Attr> getAttr(int i) {
                return Optional.empty();
            }

            @Override
            public Optional<Attr> getAttr(String s) {
                return Optional.empty();
            }
        });
        init();
    }

    /**
     * 构造参数
     *
     * @param context 上下文
     * @param attrs attrs
     */
    public PhotoDraweeView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    /**
     * 构造参数
     *
     * @param context 上下文
     * @param attrs attrs
     * @param defStyle def风格
     */
    public PhotoDraweeView(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    /**
     * 初始化
     */
    protected void init() {
        if (mAttacher == null || mAttacher.getDraweeView() == null) {
            mAttacher = new Attacher(this);
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        int saveCount = canvas.save();
        canvas.concat(mAttacher.getDrawMatrix());

        canvas.restoreToCount(saveCount);
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        init();
        super.onComponentBoundToWindow(component);
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        mAttacher.onDetachedFromWindow();
        super.onComponentUnboundFromWindow(component);
    }

    @Override
    public float getMinimumScale() {
        return mAttacher.getMinimumScale();
    }

    @Override
    public float getMediumScale() {
        return mAttacher.getMediumScale();
    }

    @Override
    public float getMaximumScale() {
        return mAttacher.getMaximumScale();
    }

    @Override
    public void setMinimumScale(float minimumScale) {
        mAttacher.setMinimumScale(minimumScale);
    }

    @Override
    public void setMediumScale(float mediumScale) {
        mAttacher.setMediumScale(mediumScale);
    }

    @Override
    public void setMaximumScale(float maximumScale) {
        mAttacher.setMaximumScale(maximumScale);
    }

    @Override
    public float getScale2() {
        return mAttacher.getScale2();
    }

    @Override
    public void setScale(float scale) {
        mAttacher.setScale(scale);
    }

    @Override
    public void setScale(float scale, boolean isAnimate) {
        mAttacher.setScale(scale, isAnimate);
    }

    @Override
    public void setScale(float scale, float focalX, float focalY, boolean isAnimate) {
        mAttacher.setScale(scale, focalX, focalY, isAnimate);
    }

    @Override
    public void setZoomTransitionDuration(long duration) {
        mAttacher.setZoomTransitionDuration(duration);
    }

    @Override
    public void setAllowParentInterceptOnEdge(boolean isAllow) {
        mAttacher.setAllowParentInterceptOnEdge(isAllow);
    }

    @Override
    public void setOnDoubleTapListener(GestureDetector.OnDoubleTapListener listener) {
        mAttacher.setOnDoubleTapListener(listener);
    }

    @Override
    public void setOnScaleChangeListener(OnScaleChangeListener listener) {
        mAttacher.setOnScaleChangeListener(listener);
    }

    @Override
    public void setOnLongClickListener(LongClickedListener listener) {
        mAttacher.setOnLongClickListener(listener);
    }

    @Override
    public void setOnPhotoTapListener(OnPhotoTapListener listener) {
        mAttacher.setOnPhotoTapListener(listener);
    }

    @Override
    public void setOnViewTapListener(OnViewTapListener listener) {
        mAttacher.setOnViewTapListener(listener);
    }

    @Override
    public OnPhotoTapListener getOnPhotoTapListener() {
        return mAttacher.getOnPhotoTapListener();
    }

    @Override
    public OnViewTapListener getOnViewTapListener() {
        return mAttacher.getOnViewTapListener();
    }

    @Override
    public void update(int imageInfoWidth, int imageInfoHeight) {
        mAttacher.update(imageInfoWidth, imageInfoHeight);
    }

    public Attacher getmAttacher() {
        return mAttacher;
    }
}
