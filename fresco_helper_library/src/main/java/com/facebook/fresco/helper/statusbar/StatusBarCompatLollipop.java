/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.statusbar;

import ohos.aafwk.ability.Ability;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;

import com.chinasoft_ohos.commontools.ohosext.animation.ValueAnimator;

/**
 * 状态栏兼容2
 *
 * @author dev
 * @since 2021-08-03
 */
public class StatusBarCompatLollipop {
    private static final int CONSTANT_129 = 129;
    private static ValueAnimator sAnimator;

    private StatusBarCompatLollipop() {
    }

    private static int getStatusBarHeight(Context context) {
        return CONSTANT_129; // 固定状态栏高度
    }

    /**
     * 设置状态栏颜色
     *
     * @param ability 能力
     * @param statusColor 状态的颜色
     */
    static void setStatusBarColor(Ability ability, int statusColor) {
        Window window = ability.getWindow();
        window.clearFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutConfig.MARK_DRAWS_SYSTEM_BAR_BACKGROUNDS); // 需要设置这个才能设置状态栏和导航栏颜色
        window.setStatusBarColor(statusColor);
        window.setStatusBarVisibility(WindowManager.LayoutConfig.SYSTEM_BAR_BRIGHT_STATUS);
    }

    /**
     * 透明状态栏
     *
     * @param ability 能力
     * @param hideStatusBarBackground 隐藏状态栏背景
     */
    static void translucentStatusBar(Ability ability, boolean hideStatusBarBackground) {
        Window window = ability.getWindow();
        if (hideStatusBarBackground) {
            window.setStatusBarColor(Color.TRANSPARENT.getValue());
            window.addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
            window.setStatusBarVisibility(Component.VISIBLE);
        } else {
            window.addFlags(WindowManager.LayoutConfig.MARK_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }
    }

    /**
     * 开始颜色动画
     *
     * @param startColor 开始颜色
     * @param endColor 最终的颜色
     * @param duration 持续时间
     * @param window 窗口
     */
    static void startColorAnimation(int startColor, int endColor, long duration, final Window window) {
        if (sAnimator != null) {
            sAnimator.cancel();
        }
        sAnimator = ValueAnimator.ofInt(startColor, endColor);
        sAnimator.setDuration(duration);
        sAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(AnimatorValue animatorValue, float v1, Object object) {
                if (window != null) {
                    window.setStatusBarColor((Integer) object);
                }
            }
        });
        sAnimator.start();
    }

}
