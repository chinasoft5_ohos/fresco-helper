/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.statusbar;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;

/**
 * 状态栏兼容1
 *
 * @author dev
 * @since 2021-08-03
 */
public class StatusBarCompatKitKat {
    private static final String TAG_FAKE_STATUS_BAR_VIEW = "statusBarView";
    private static final String TAG_MARGIN_ADDED = "marginAdded";
    private static final int CONSTANT_129 = 129;

    private StatusBarCompatKitKat() {
    }

    private static int getStatusBarHeight(Context context) {
        return CONSTANT_129; // 固定状态栏高度
    }


    /**
     * 添加假状态栏视图
     * <p>
     * 1. Add fake statusBarView.
     * 2. set tag to statusBarView.
     *
     * @param ability 能力
     * @param statusBarColor 状态栏颜色
     * @param statusBarHeight 状态栏的高度
     * @return {@link Component}
     */
    private static Component addFakeStatusBarView(Ability ability, int statusBarColor, int statusBarHeight) {
        Window window = ability.getWindow();
        Component mStatusBarView = new Component(ability);
        StackLayout.LayoutConfig layoutParams =
            new StackLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, statusBarHeight);
        layoutParams.alignment = LayoutAlignment.TOP;
        mStatusBarView.setLayoutConfig(layoutParams);
        mStatusBarView.setTag(TAG_FAKE_STATUS_BAR_VIEW);
        window.clearFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutConfig.MARK_DRAWS_SYSTEM_BAR_BACKGROUNDS); // 需要设置这个才能设置状态栏和导航栏颜色
        window.setStatusBarColor(statusBarColor);
        ComponentContainer component = (ComponentContainer) ability.getCurrentFocus();
        component.addComponent(mStatusBarView);
        return mStatusBarView;
    }

    /**
     * use reserved order to remove is more quickly.
     *
     * @param ability 能力
     */
    private static void removeFakeStatusBarViewIfExist(Ability ability) {
        ComponentContainer mDecorView = (ComponentContainer) ability.getCurrentFocus();
        Component fakeView = (Component) mDecorView.getTag();
        if (fakeView != null) {
            mDecorView.removeComponent(fakeView);
        }
    }

    /**
     * add marginTop to simulate set FitsSystemWindow true
     *
     * @param contentChild 内容的孩子
     * @param statusBarHeight 状态栏的高度
     */
    private static void addMarginTopToContentChild(Component contentChild, int statusBarHeight) {
        if (contentChild == null) {
            return;
        }
        if (!TAG_MARGIN_ADDED.equals(contentChild.getTag())) {
            StackLayout.LayoutConfig lp = (StackLayout.LayoutConfig) contentChild.getLayoutConfig();
            lp.setMarginTop(lp.getMarginTop() + statusBarHeight);
            contentChild.setLayoutConfig(lp);
            contentChild.setTag(TAG_MARGIN_ADDED);
        }
    }

    /**
     * remove marginTop to simulate set FitsSystemWindow false
     *
     * @param contentChild 内容的孩子
     * @param statusBarHeight 状态栏的高度
     */
    private static void removeMarginTopOfContentChild(Component contentChild, int statusBarHeight) {
        if (contentChild == null) {
            return;
        }
        if (TAG_MARGIN_ADDED.equals(contentChild.getTag())) {
            StackLayout.LayoutConfig lp = (StackLayout.LayoutConfig) contentChild.getLayoutConfig();
            lp.setMarginTop(lp.getMarginTop() - statusBarHeight);
            contentChild.setLayoutConfig(lp);
            contentChild.setTag(null);
        }
    }

    /**
     * 设置状态栏颜色
     *
     * @param ability 能力
     * @param statusColor 状态的颜色
     */
    static void setStatusBarColor(Ability ability, int statusColor) {
        Window window = ability.getWindow();
        window.addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        ComponentContainer mContentView = (ComponentContainer) ability.getCurrentFocus();
        Component mContentChild = mContentView.getComponentAt(0);
        int statusBarHeight = getStatusBarHeight(ability);

        removeFakeStatusBarViewIfExist(ability);
        addFakeStatusBarView(ability, statusColor, statusBarHeight);
        addMarginTopToContentChild(mContentChild, statusBarHeight);

//        if (mContentChild != null) {
//            mContentChild.setFitsSystemWindows(mContentChild, false);
//        }
    }

    /**
     * 透明状态栏
     *
     * @param ability 能力
     */
    static void translucentStatusBar(Ability ability) {
        Window window = ability.getWindow();
        window.addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        ComponentContainer mContentView = (ComponentContainer) ability.getCurrentFocus();
        Component mContentChild = mContentView.getComponentAt(0);

        removeFakeStatusBarViewIfExist(ability);
        removeMarginTopOfContentChild(mContentChild, getStatusBarHeight(ability));
//        if (mContentChild != null) {
//            mContentChild.setFitsSystemWindows(mContentChild, false);
//        }
    }


//    static void setStatusBarColorForCollapsingToolbar(Ability activity, final AppBarLayout appBarLayout, final CollapsingToolbarLayout collapsingToolbarLayout,
//                                                      Toolbar toolbar, int statusColor) {
//        Window window = activity.getWindow();
//        window.addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
//        ComponentContainer mContentView = (ComponentContainer) window.getCurrentComponentFocus().get();
//
//        Component mContentChild = mContentView.getComponentAt(0);
////        mContentChild.setFitsSystemWindows(false);
//        ((Component) appBarLayout.getParent()).setFitsSystemWindows(false);
//        appBarLayout.setFitsSystemWindows(false);
//        collapsingToolbarLayout.setFitsSystemWindows(false);
//        collapsingToolbarLayout.getChildAt(0).setFitsSystemWindows(false);
//
//        toolbar.setFitsSystemWindows(false);
//        if (toolbar.getTag() == null) {
//            CollapsingToolbarLayout.LayoutParams lp = (CollapsingToolbarLayout.LayoutParams) toolbar.getLayoutParams();
//            int statusBarHeight = getStatusBarHeight(activity);
//            lp.height += statusBarHeight;
//            toolbar.setLayoutParams(lp);
//            toolbar.setPadding(toolbar.getPaddingLeft(), toolbar.getPaddingTop() + statusBarHeight, toolbar.getPaddingRight(), toolbar.getPaddingBottom());
//            toolbar.setTag(true);
//        }
//
//        int statusBarHeight = getStatusBarHeight(activity);
//        removeFakeStatusBarViewIfExist(activity);
//        removeMarginTopOfContentChild(mContentChild, statusBarHeight);
//        final View statusView = addFakeStatusBarView(activity, statusColor, statusBarHeight);
//
//        CoordinatorLayout.Behavior behavior = ((CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams()).getBehavior();
//        if (behavior != null && behavior instanceof AppBarLayout.Behavior) {
//            int verticalOffset = ((AppBarLayout.Behavior) behavior).getTopAndBottomOffset();
//            if (Math.abs(verticalOffset) > appBarLayout.getHeight() - collapsingToolbarLayout.getScrimVisibleHeightTrigger()) {
//                statusView.setAlpha(1f);
//            } else {
//                statusView.setAlpha(0f);
//            }
//        } else {
//            statusView.setAlpha(0f);
//        }
//
//        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
//            @Override
//            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
//                if (Math.abs(verticalOffset) > appBarLayout.getHeight() - collapsingToolbarLayout.getScrimVisibleHeightTrigger()) {
//                    if (statusView.getAlpha() == 0) {
//                        statusView.animate().cancel();
//                        statusView.animate().alpha(1f).setDuration(collapsingToolbarLayout.getScrimAnimationDuration()).start();
//                    }
//                } else {
//                    if (statusView.getAlpha() == 1) {
//                        statusView.animate().cancel();
//                        statusView.animate().alpha(0f).setDuration(collapsingToolbarLayout.getScrimAnimationDuration()).start();
//                    }
//                }
//            }
//        });
//    }
}
