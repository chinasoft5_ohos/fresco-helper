/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.facebook.fresco.helper.loading;

import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.ColorFilter;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.global.resource.Resource;
import ohos.media.image.PixelMap;

import com.oszc.bbhmlibrary.wrapper.RectF;

/**
 * 图像加载元素
 *
 * @author dev
 * @since 2021-08-02
 */
public class ImageLoadingElement extends PixelMapElement {
    private Paint mRingBackgroundPaint;
    private int mRingBackgroundColor;
    /**
     * 画圆环的画笔
     */
    private Paint mRingPaint;
    /**
     * 圆环颜色
     */
    private int mRingColor;
    /**
     * 半径
     */
    private float mRadius;
    /**
     * 圆环半径
     */
    private float mRingRadius;
    /**
     * 圆环宽度
     */
    private float mStrokeWidth;
    /**
     * 圆心x坐标
     */
    private int mXCenter;
    /**
     * 圆心y坐标
     */
    private int mYCenter;
    /**
     * 总进度
     */
    private int mTotalProgress = 10000;
    /**
     * 当前进度
     */
    private int mProgress;

    /**
     * 图像加载元素
     *
     * @param pixelMap 像素映射
     */
    public ImageLoadingElement(PixelMap pixelMap) {
        super(pixelMap);
        initAttrs();
    }

    private void initAttrs() {
        mRadius = 100;
        mStrokeWidth = 10;
        mRingBackgroundColor = 0xFFadadad;
        mRingColor = 0xFF0EB6D2;
        mRingRadius = mRadius + mStrokeWidth / 2;
        initVariable();
    }

    private void initVariable() {
        mRingBackgroundPaint = new Paint();
        mRingBackgroundPaint.setAntiAlias(true);
        mRingBackgroundPaint.setColor(new Color(mRingBackgroundColor));
        mRingBackgroundPaint.setStyle(Paint.Style.STROKE_STYLE);
        mRingBackgroundPaint.setStrokeWidth(mStrokeWidth);

        mRingPaint = new Paint();
        mRingPaint.setAntiAlias(true);
        mRingPaint.setColor(new Color(mRingColor));
        mRingPaint.setStyle(Paint.Style.STROKE_STYLE);
        mRingPaint.setStrokeWidth(mStrokeWidth);
    }

    @Override
    public void drawToCanvas(Canvas canvas) {
        drawBar(canvas, mTotalProgress, mRingBackgroundPaint);
        drawBar(canvas, mProgress, mRingPaint);
    }

    private void drawBar(Canvas canvas, int level, Paint paint) {
        if (level > 0) {
            Rect bound = getBounds();
            mXCenter = bound.getCenterX();
            mYCenter = bound.getCenterY();
            RectF oval = new RectF();
            oval.left = mXCenter - mRingRadius;
            oval.top = mYCenter - mRingRadius;
            oval.right = mRingRadius * 2 + (mXCenter - mRingRadius);
            oval.bottom = mRingRadius * 2 + (mYCenter - mRingRadius);
            Arc arc = new Arc(-90, ((float) level / mTotalProgress) * 360, false);
            canvas.drawArc(oval, arc, paint); //
        }
    }

    /**
     * 水平变化
     *
     * @param level 水平
     * @return boolean
     */
    protected boolean onLevelChange(int level) {
        mProgress = level;
        if (level > 0 && level < 10000) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void setAlpha(int alpha) {
        mRingPaint.setAlpha(alpha);
    }

    /**
     * 设置滤色器
     *
     * @param cf cf
     */
    public void setColorFilter(ColorFilter cf) {
        mRingPaint.setColorFilter(cf);
    }

}
